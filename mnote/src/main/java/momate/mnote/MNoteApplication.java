package momate.mnote;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MNoteApplication {
    public static void main(String[] args) {
        SpringApplication.run(MNoteApplication.class,args);
    }
}
