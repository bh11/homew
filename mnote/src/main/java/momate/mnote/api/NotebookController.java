package momate.mnote.api;

import momate.mnote.entity.Notebook;
import momate.mnote.repository.NotebookRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api/notebooks")
public class NotebookController {

    @Autowired
    private NotebookRepository notebookRepository;

    @GetMapping("/all")
    public List<Notebook> all(){
        return notebookRepository.findAll();

    }

}
