package momate.mnote.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.UUID;

@Entity(name = "Note")
@Table(name = "note")
public class Note implements Serializable {

    @Id
    @Column(name = "noteId")
    private UUID id;
    @Column(name = "title")
    private String title;
    @Column(name = "text")
    private String text;


    @ManyToOne(fetch = FetchType.LAZY)
    private Notebook notebook;
    @Column(name = "lastModifiedDate")
    private Date lastModifiedDate;

    protected Note(){
        this.id = UUID.randomUUID();
        this.lastModifiedDate = new Date();
    }

    public Note(String title, String text, Notebook notebook){
        this();
        this.title = title;
        this.text = text;
        this.notebook = notebook;
    }

    public void setLastModifiedDate(Date lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }

    public Note(String id, String title, String text, Notebook notebook) {
        this(title, text, notebook);
        if (id != null) {
            this.id = UUID.fromString(id);
        }
    }


        public UUID getId() {
            return id;
        }

        public String getTitle() {
            return title;
        }

        public String getText() {
            return text;
        }

        public Notebook getNotebook() {
            return notebook;
        }

        public Date getLastModifiedDate() {
            return lastModifiedDate;
        }

    @Override
    public String toString() {
        return "Note{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", text='" + text + '\'' +
                ", notebook=" + notebook +
                ", lastModifiedDate=" + lastModifiedDate +
                '}';
    }
}
