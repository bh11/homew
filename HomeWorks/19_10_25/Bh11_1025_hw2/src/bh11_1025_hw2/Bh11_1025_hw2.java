package bh11_1025_hw2;

import java.util.Scanner;


public class Bh11_1025_hw2 {


    public static void main(String[] args) {
        /*
        2. 
kérjük be egy pozitív egész számot (ellenőrzés), majd írjunk ki csillagokat az alábbi formában a megadott szám alapján: 
a. 
szam = 7
*******
******
*****
****
***
**
*
*/
        
        Scanner sc = new Scanner (System.in);
        boolean isNum = false;
        int number = 0;
        
        do{
            System.out.println("Adj meg egy egész számot: ");
            if (sc.hasNextInt()) {
                number = sc.nextInt();
                isNum = true;
            }else{
            sc.next();
            }
        }while(!isNum);
        
        for (int i = 0; i <= number; i++) {
            for (int j = number; j >= i; j--) {
                System.out.print("*");
            }
            System.out.println("");
                
            
           
        }
    }

}
