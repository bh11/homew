package bh11_1025_hw1;


public class Bh11_1025_hw1 {


    public static void main(String[] args) {
        /*
1. tomb, 50 elem, [-100, 100] értékkészlet, írjuk ki a következőket: 
 - maximum hely és érték,
 - minimum hely és érték, 
 - átlag 
 - összeg
*/
        
        int[] arr = new int[50];
        int max = arr[0];
        int maxIndex=0;
        for (int i = 0; i < arr.length; i++) {
            arr[i] = - 100 + (int)(Math.random()*(100 - (-100))+1); 
        }
        
        for (int i = 0; i < arr.length; i++) {
            if(arr[i] > max){
            max = arr[i];
            maxIndex = i;
            }
        }
        
        System.out.println("A tömb elemei : ");
        for (int i = 0; i < arr.length; i++) {
            System.out.print(arr[i]);
            System.out.print(", ");
        }
        System.out.println("\nA tömb max értéke: " + max);
        System.out.println("A tömb max értékenek helye: " + maxIndex);
    }

}
