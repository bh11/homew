1. feladat
- A program nem fut le. Az if után egy = jel áll, így nem tudja megvizsgálni a feltételt. 
ok
2. feladat
- A program nem fut le. Csak a j változó lett inicializálva. 
ok
3. feladat
- A program "break" hiányában a "B" és "C" karakterket írja ki. 10/11=0 maradék: 10
ok
4. feladat
- A program "break" hiányában kiírja az "A", "B" és "C" karaktereket. 

5. feladat
- A program nem fut le, long típusú változóban int típusú szám lett inicializálva. 

6. feladat
- A feltétel igaz így a program kiírja az "A", majd a "B" karatert és a "11" számot. 

7. feladat
- A feltétel hamis. Így a program csak a "B" karakter és a "11" számot írja ki.

8. feladat
- A feltétel igaz így a program kiírja az "A", majd a "B" karatert és a "11" számot. 

9. feladat
- A feltétel igaz így a program kiírja az "A", majd a "B" karatert és a "11" számot. 

10. feladat
- A program nem fut le. If fetétel után hiányzik a "{}".

-- lefut az if utáni első utasítás
11.feladat
- A program nem fut le. Az if feltétel után ";" van és hiányzik a "{}"

-- a ; egy üres utastás így mind a ketőtt kiírja

12. feladat
- A program nem fut le. A while fetétel után ";" van.

-- kiírja az A-t

13. feladat
- A program kiírja "i + j". Időzőjelek között nem történik aritmetikai művelet.

14. feladat
- A program nem fut le a "j" változó csak a két zárójel között létezik.

15. feladat
- A program nem fut le. Helytelen kasztolás -> short c = (short) (a+b);

16. feladat
- A program nem fut le. Helytelen for ciklus
 
17. feladat
- A program nem fut le. Helytelen for ciklus

-- lefut random fut le, ha kisebb 0,5nél kisebb lefut majd újra ha nagyobb le se fut
18. feladat 
-  A program nem fut le. Helytelen for ciklus

-- nem csak a for-on belül lehet inicializálni az "i"-t