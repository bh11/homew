CREATE TABLE archive (
    id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
    hotel_id VARCHAR(15),
    guest_id VARCHAR(15) ,
    date_from DATE,
    date_to DATE,
    room_id VARCHAR(15),
    FOREIGN KEY (hotel_id)
        REFERENCES hotel (id),
    FOREIGN KEY (guest_id)
        REFERENCES guest (id),
    FOREIGN KEY (room_id)
        REFERENCES room (id)
);
