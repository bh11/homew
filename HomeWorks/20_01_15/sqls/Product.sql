CREATE TABLE regisztráció (
    dkód INT NOT NULL,
    okód VARCHAR(10) NOT NULL,
    dátum DATE,
    FOREIGN KEY (dkód)
        REFERENCES diákok (dkód),
    FOREIGN KEY (okód)
        REFERENCES órák (okód)
);

