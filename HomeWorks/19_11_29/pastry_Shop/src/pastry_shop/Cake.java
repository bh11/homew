package pastry_shop;

public class Cake extends Sweets {

    private String[] componentsOfCake = new String[5];

    public Cake(String fantasyName, int numberOfCalories) {
        super(fantasyName, numberOfCalories);
    }

    public int generateRandomNumber(int min, int max) {

        return (int) (Math.random() * (max - min) + min);
    }
    

    public String randomIngredient() {
        String[] ingr = {"egg", "milk", "butter", "flour", "carrot", "sugar", "candy", "fruit"};
        return ingr[generateRandomNumber(0, ingr.length-1)];
    }

    @Override
    public String recommendedServingMode() {
        return "It is recommended to serve it on a plate";
    }

}
