package pastry_shop;

import java.util.HashSet;
import java.util.Set;

public class BirthdayCake extends Cake {

    private String birthdayWishes;
    Set<String> componentsOfBirthdayCake = new HashSet<>();

    public BirthdayCake(String fantasyName, int numberOfCalories, String[] ingredients) {
        super(fantasyName, numberOfCalories);
        for (int i = 5; i < ingredients.length; i++) {
            this.ingredients.add(ingredients[i]);
        }
    }

    


    public String getBithdayWishes() {
        return birthdayWishes;
    }

    public void setBithdayWishes(String bithdayWishes) {
        this.birthdayWishes = bithdayWishes;
    }

    public Set<String> getComponentsOfBirthdayCake() {
        return componentsOfBirthdayCake;
    }

    public void setComponentsOfBirthdayCake(Set<String> componentsOfBirthdayCake) {
        this.componentsOfBirthdayCake = componentsOfBirthdayCake;
    }
    
}
