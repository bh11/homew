package pastry_shop;

public abstract class Sweets {

    private int numberOfCalories;
    private String fantasyName;

    public Sweets(String fantasyName, int numberOfCalories ) {
        this.numberOfCalories = numberOfCalories;
        this.fantasyName = fantasyName;
    }
    
    
   
    
    public abstract String recommendedServingMode();

    public int getNumberOfCalories() {
        return numberOfCalories;
    }

    public void setNumberOfCalories(int numberOfCalories) {
        this.numberOfCalories = numberOfCalories;
    }

    public String getFantasyName() {
        return fantasyName;
    }

    public void setFantasyName(String fantasyName) {
        this.fantasyName = fantasyName;
    }
    
    
}
