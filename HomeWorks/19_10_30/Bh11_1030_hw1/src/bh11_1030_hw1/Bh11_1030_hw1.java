package bh11_1030_hw1;

import java.util.Scanner;



public class Bh11_1030_hw1 {
    // eltároljuk globális változókban az alap értékeket.
    // konstans értékek megadása - final kulcsszóval azok az adatok amiket nem lehet megváltoztatani / Final - letiltja az értékadó operátort
    // Final- más névadási konvenció - minden nagybetű szavak között "_"
    static final Scanner SC = new Scanner(System.in);
    
    static final int MIN_SIZE = 10;
    static final int MAX_SIZE = 15;
    static final int MIN_NUMBER_OF_BOMBS = 8;
    static final int MAX_NUMBER_OF_BOMBS = 20;
    
    static final char GOOD_BOMBS = 'D';
    static final char BAD_BOMBS = '*';
    static final char GIFT = '@';
    static final char PLAYER = 'T';
    
    static final char EMPTY = '\0';
    
    static int lifePoints = 5;
    static int playerX = 0;
    static int playerY=0;
    

    // a blokkok csak egy dolgot csináljanak, ne legyen "és"
    static int getNumber(int from, int to){
        do {
            System.out.printf("Give me a number between %d and %d: \n" , from , to );
            if (SC.hasNextInt()) {
                int number = SC.nextInt();
                if (number >= from && number <= to) {
                    return number;
                }
            }else{
            swallowString();
            }
        } while (true);  
    }
    // ki szervezzük az sc.next -et így olvashatóbb 
    static void swallowString(){
    SC.next();
    }
    // kiírjuk a pályát
    static void printField(char[][] field){
        for (int i = 0; i < field.length; i++) {
            for (int j = 0; j < field[i].length; j++) {
                if (field[i][j] == EMPTY) {
                    System.out.print(" ");
                }else{
                System.out.print(field[i][j]);
                } 
            }
            System.out.println();
        }
    }
    
    
   static void currentLifePonts(){
       System.out.println("Life: " + lifePoints);
   }

    static void putPlayer(char[][] field){
    field[playerX][playerY] = PLAYER;
    }
    
     // létrehozzuk a pályát
    static void play(int row, int col) {
        char[][] field = new char[row][col];

        putPlayer(field);
        printField(field);
        putBombs(field);
        
        step(field);
    }
    
    static void putBombs(char[][] field){
    int numberOfBombs = getNumber(MIN_NUMBER_OF_BOMBS, MAX_NUMBER_OF_BOMBS);
    // kiszánoljuk a jó és rossz bombák arányát
    int numberOfGoodBombs = calculateGoodBombs(numberOfBombs);
    int numberOfBadBombd = numberOfBombs - numberOfGoodBombs;
        setBombs(field, numberOfGoodBombs, GOOD_BOMBS);
        setBombs(field, numberOfBadBombd, BAD_BOMBS);
        setBombs(field, 3, GIFT);
        
        printField(field);
    }
    // kiszámoljuk a bombák 30%át a jó bombáknak 
    static int calculateGoodBombs(int numberOfBombs){
    return numberOfBombs / 3;
    }
    /**
     * This method returns a random number between from and to excluding to
     * @param from
     * @param to
     * @return 
     */
    static int generateRandomNumber(int from, int to){
    return (int)(Math.random()* (to-from) +from);
    }
    
    static void setBombs(char[][] field, int count, char character){
        for (int i = 0; i < count; i++) {
            int x = generateRandomNumber(0, field.length);
            int y = generateRandomNumber(0, field[i].length);
            if (field[x][y] == EMPTY) {
                field[x][y] = character;
                
            }else{
            i--;
            }
            }
    }
    // todo - lehessen jobbra / balra / fel / le mozogni
    static void step(char[][] field){
        while(lifePoints > 0 && !isPLayerInTheLAstRow(field)){
            char c = SC.next().charAt(0);
            
            switch(c){
                case 'r' : stepRight(field);
                break;
                  case 'd' : stepDown(field);
                break;
                  case 'l' : stepLeft(field);
                  break;
                  case 'u' : stepUp(field);
            }
            printField(field);
        }
    }
    
    
    static void stepRight(char[][] field){
        if (!isPLayerInTheLAstColum(field)) {
        handleBombs(field, playerX, playerY + 1);
            if (lifePoints > 0) {
                field[playerX][playerY] = EMPTY;
                playerY++;
                field[playerX][playerY] = PLAYER;
            }
        }
        currentLifePonts();
    }
        
    static void stepLeft(char[][] field){
        if (!isPLayerInTheFirstColum(field)) {
                    handleBombs(field, playerX, playerY -1 );
            if (lifePoints > 0) {
                field[playerX][playerY] = EMPTY;
                playerY--;
                field[playerX][playerY] = PLAYER;
            }
            
        }
        currentLifePonts();
    }
        
    static void stepDown(char[][] field){
        if (!isPLayerInTheLAstRow(field)) {
                    handleBombs(field, playerX + 1, playerY );
            if (lifePoints > 0) {
                field[playerX][playerY] = EMPTY;
                playerX++;
                field[playerX][playerY] = PLAYER;
            }
            
        }
        currentLifePonts();
    }
    
    static void stepUp(char[][] field){
        if (!isPLayerInTheFirstRow(field)) {
                    handleBombs(field, playerX - 1, playerY );
            if (lifePoints > 0) {
                field[playerX][playerY] = EMPTY;
                playerX--;
                field[playerX][playerY] = PLAYER;
            }
            
        }
        currentLifePonts();
    }

    
    static void handleBombs(char[][] field, int x, int y){
        switch (field[x][y]) {
            case GOOD_BOMBS:  lifePoints--;
            break;
            case GIFT: lifePoints++;
            break;
            case BAD_BOMBS: lifePoints = 0;
                break;
        }
    }
    
    static boolean isPLayerInTheLAstRow(char[][] field){
    return playerX == field.length -1;
    }
    
    static boolean isPLayerInTheLAstColum(char[][] field){
    return playerY == field[0].length -1;
    }
    
    static boolean isPLayerInTheFirstColum(char[][] field){
    return playerY == (field[0].length -1) - (field[0].length -1);
    }
    
    static boolean isPLayerInTheFirstRow(char[][] field){
    return playerX == (field.length -1) -(field.length -1);
    }
    
    

    public static void main(String[] args) {
        
        int row = getNumber(MIN_SIZE, MAX_SIZE);
        int col = getNumber(MIN_SIZE, MAX_SIZE);
        play(row, col);
    }

}
