package bh11_1030_hw4;


public class Bh11_1030_hw4 {

    static void reverseArr(int[] nums){
        for (int i = nums.length-1; i > 0; i--) {
            System.out.print(nums[i] + " ");
        }
    
    }

    public static void main(String[] args) {
        /*
        Írj függvényt, ami egy tömböt átvesz paraméterként és hátulról indulva kiírja minden második elemét!
        Ügyelj arra, hogy nehogy túl/alulindexeld a tömböt!
        */
        int[] numbers = {2,14,23,-12,56,3};
        reverseArr(numbers);
    }

}
