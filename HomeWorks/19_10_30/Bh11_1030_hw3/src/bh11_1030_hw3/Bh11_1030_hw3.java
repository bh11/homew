package bh11_1030_hw3;


public class Bh11_1030_hw3 {
    
static int highAbs(int a, int b){
return (Math.abs(a) > Math.abs(b) ? a : b);

}

    public static void main(String[] args) {
        /*
        Írj függvényt, amely paraméterként átvesz két egész számot és visszaadja azt, amelyiknek az abszolút
        értéke nagyobb!
        */
        System.out.println(highAbs(-21, 40));
       
    }

}
