package bh11_1030_hw6;

import java.util.Scanner;


public class Bh11_1030_hw6 {
       
    static final Scanner SC = new Scanner(System.in);
    
    static int rectengleSize(){
        int number = 0;
        
        if (SC.hasNextInt()) {
            number= SC.nextInt();
        }else{
        SC.next();
    }
        
         return number;
    }

    static int rectengleArea(int a, int b){
    
    return a*b;
    }

    public static void main(String[] args) {

        /*
        Készítsünk olyan függvényt, amely egy téglalap két oldalának ismeretében kiszámítja a téglalap területét!
Az eredmény legyen a visszatérési érték. Ehhez írjunk programot, amiben ki is próbáljuk a függvényt.
        */
        System.out.println("Input the two length of rectangle's sides:  ");
        System.out.println(rectengleArea(rectengleSize(),rectengleSize()));
    }

}
