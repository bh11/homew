package bh11_1030_hw2;

import java.util.Scanner;


public class Bh11_1030_hw2 {
    
   static final Scanner SC =new Scanner(System.in);
    
    static int getNumber(){
    int number = 0;
    boolean isNum= false;
        do {
            
            if (SC.hasNextInt()) {
                number = SC.nextInt();
                if (number > 0) {
                    isNum = true;
                }
            }else{
            SC.next();
            }
        } while (!isNum);
        
    return number;
    }
    
    static boolean isPrime(int a){
    return (a % a == 0 && a % 1 == 0 );
    }
    
    static void printResult(int nth){
        int count =  0;
        int num = 1;
        int i;
        while(count < nth){
        num++;
            for (i = 2; i < num; i++) {
                if (num % i == 0) {
                    break;
                }
            }
            if (i == num) {
                count++;
            }
        }
        System.out.printf("The %dth prime number is %d.\n", nth , num);
    
    }

    public static void main(String[] args) {
        
        /*
        Írj függvényt, amely a paramétereként átvett pozitív, egész számról eldönti, hogy az prím-e! Logikai
visszatérése legyen, amely akkor IGAZ, ha a paraméterként átvett szám prím, különben HAMIS.
Írj programot, amely a felhasználótól bekér egy számot, és a fenti függvény segítségével kiírja a
képernyőre az annyiadik prímszámot.
        */
      System.out.println("Input a number: ");              
      printResult(getNumber());
    }

}
