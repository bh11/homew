package bh11_1030_hw7;




public class Bh11_1030_hw7 {

    static void randomArr(int[] nums){
        for (int i = 0; i < 20; i++) {
            nums[i] = (int)(Math.random()*100);
        }
    }
    
    static int canDivide (int[] nums){
        int count=0;
        for (int i = 0; i < nums.length; i++) {
            if(nums[i] % 3 == 0 && nums[i] % 5 != 0){
            count++;
            }
        }
        
        return count;
    }
    
    public static void main(String[] args) {
        /*
        Készítsünk olyan függvényt, amely meghatározza egy tömbben hány 3-al osztható, de 5-el nem osztható
        szám van!
        */
        
        int[] nums = new int[20];
        randomArr(nums);
        System.out.println("The random array's elements are: ");
        for (int i : nums) {
            System.out.print(i + " ");
        }
        System.out.println();
        System.out.println("From these can be devided with 3 and can't be devide with 5: " + canDivide(nums));

    }

}
