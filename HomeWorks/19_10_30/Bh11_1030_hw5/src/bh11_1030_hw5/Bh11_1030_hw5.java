package bh11_1030_hw5;

import java.util.Scanner;


public class Bh11_1030_hw5 {
    
    static char inputChar(){
        
        Scanner sc = new Scanner(System.in);
        char a = sc.next().charAt(0);
        return a;
    }
    static char upperCase(char a){
    return Character.toUpperCase(a);
    
    }

    public static void main(String[] args) {
        
        /*
        Írj egy függvényt, ami a paraméterként átvett kis betűnek megfelelő nagybetűt adja vissza!
        */
        System.out.println("Input a lowercase character: ");
        System.out.println(upperCase(inputChar()));
    }

}
