package namethreads;

public class NameThreads {

    public static void main(String[] args) throws InterruptedException {

        NameWriter n1 = new NameWriter("n1 Molnár Máté", "MOMATE");
        NameWriter n2 = new NameWriter("n2 Molnár Máté", "MOMATE");
        NameWriter n3 = new NameWriter("n3 Molnár Máté", "MOMATE");
        NameWriter n4 = new NameWriter("n4 Molnár Máté", "MOMATE");
        NameWriter n5 = new NameWriter("n5 Molnár Máté", "MOMATE");

        n1.start();
        n1.join();
        n2.start();
        n2.join(1000);
        n3.start();
        n3.join();
        n4.start();
        n5.start();
        
        // az n2-t nem érjük el soha
        
        
    }

}
