package namethreads;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

public class NameWriter extends Thread {

    private final String name;
    private final String neptunId;

    public NameWriter(String name, String neptunId) {
        this.name = name;
        this.neptunId = neptunId;
    }

    @Override
    public void run() {
        while (true) {
            try {
                Thread.sleep(1000);
            
            System.out.println("Név: " + this.name + ", NeptunCode: " + this.neptunId);
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Date date = new Date(System.currentTimeMillis());
            System.out.println(sdf.format(date));
            } catch (InterruptedException ex) {
                System.out.println(ex);
            }
        }
    }
}
