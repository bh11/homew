package employee;



public class EmployeeFactory {

    public AbstractEmployee createEmployee(String type) {
        if ("worker".equalsIgnoreCase(type)) {
            return new Worker();
        } else if ("boss".equalsIgnoreCase(type)) {
            return new Boss();
        }
        return null;

    }

}
