package employee;

public class AbstractEmployee {

    private String name;

    public AbstractEmployee() {
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return "AbstractEmployee{" + "name=" + name + '}';
    }

    public void setName(String name) {
        this.name = name;
    }

}
