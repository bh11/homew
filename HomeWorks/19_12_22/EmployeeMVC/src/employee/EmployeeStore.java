package employee;

import java.util.ArrayList;
import java.util.List;

public class EmployeeStore {

    private List<AbstractEmployee> employees = new ArrayList<>();
    private EmployeeFactory emp = new EmployeeFactory();

    public String printEmployees() {
        return employees.toString();
    }

    public void createBoss(String bossName) {
        
        AbstractEmployee boss = emp.createEmployee("boss");
        employees.add(boss);
        boss.setName(bossName);
        
    }

    public void createWorker(String workerName) {
        
        AbstractEmployee worker = emp.createEmployee("worker");
        employees.add(worker);
        worker.setName(workerName);
    }

}
