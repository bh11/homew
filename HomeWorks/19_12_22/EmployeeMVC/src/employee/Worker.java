package employee;

public class Worker extends AbstractEmployee{

    private Boss chief;
    private String name;
    
    public Worker(){}
    
    



    public Boss getChief() {
        return chief;
    }

    public void setChief(Boss chief) {
        this.chief = chief;
    }

    @Override
    public String toString() {
        return "Worker{" + "chief=" + chief + '}';
    }
    
    
    
    
}
