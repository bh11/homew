package employee;

import java.util.ArrayList;
import java.util.List;

public class Boss extends AbstractEmployee{

    private String name;
    
    private List<Worker> workers = new ArrayList<>();
    
    public Boss(){}
    
 
    @Override
    public void setName(String name) {
        this.name = name;
    }

    
    public List<Worker> getWorkers() {
        return workers;
    }

    public void setWorkers(List<Worker> workers) {
        this.workers = workers;
    }

    @Override
    public String toString() {
        return "Boss{" + "workers=" + workers + '}';
    }
    
    

    
    

}
