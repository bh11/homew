package view;

import controller.Controller;
import java.util.Scanner;

public class ConsoleView implements View {

    private static final String END = "exit";

    private Controller controller;

    private void consoleReader() {
        try (Scanner sc = new Scanner(System.in)) {
            String input;

            do {
                System.out.println("Input type of Emloyees (Boss | Worker )\n| Report | Save | exit -> stop program): ");
                input = sc.nextLine();

                switch (input) {
                    case "Boss":
                        System.out.println("Input the name: ");
                        String bossName;
                        bossName = sc.nextLine();
                        controller.setBossName(bossName);
                        break;
                    case "Worker":
                        System.out.println("Input the name: ");
                        String workerName = sc.nextLine();
                        controller.setWorkerName(workerName);
                        break;
                    case "Report":
                       controller.getEmployees();
                        break;
                    case "Save":
                        break;

                }

            } while (!END.equals(input));
        }
    }

    @Override
    public void setController(Controller con) {
        con = controller;
    }

    @Override
    public void start() {
        consoleReader();
    }



}
