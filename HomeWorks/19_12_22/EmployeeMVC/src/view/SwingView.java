package view;

import controller.Controller;
import java.awt.BorderLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class SwingView extends JFrame implements View {

    private Controller controller;

    private final JButton add = new JButton("Add");
    private final JButton report = new JButton("Report");
    private final JButton save = new JButton("Save");

    private final JTextField boss = new JTextField(15);
    private final JTextField worker = new JTextField(15);
    private final JTextField list = new JTextField(80);

    private JPanel buildNorth() {
        JLabel bossLabel = new JLabel("Boss:");
        JLabel workerLabel = new JLabel("Worker:");
        JPanel north = new JPanel();

        north.add(bossLabel);
        north.add(boss);
        north.add(workerLabel);
        north.add(worker);

        return north;
    }

    private JPanel buildSouth() {
        JLabel toLabel = new JLabel("Employees:");
        JPanel south = new JPanel();

        south.add(toLabel);
        south.add(list);
        south.add(save);

        return south;
    }

    private JPanel buildCenter() {
        JPanel center = new JPanel();
        center.add(add);
        center.add(report);
        
        add.addActionListener( l -> controller.getEmployees());
        
        add.addActionListener(l -> {
            controller.setBossName(boss.getText());
            controller.setWorkerName(worker.getText());
            
        });

        return center;
    }

    private void buildWindow() {
        
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        add(buildNorth(), BorderLayout.NORTH);
        add(buildCenter(), BorderLayout.CENTER);
        add(buildSouth(), BorderLayout.SOUTH);
        pack();
        setVisible(true);
    }

    @Override
    public void setController(Controller con) {
        con = controller;
    }

    @Override
    public void start() {
        buildWindow();
    }

}
