package view;

import controller.Controller;

/**
 *
 * @author momate
 */
public interface View {
    
    void setController(Controller con);
   
    void start();
    
}
