package controller;

import model.Model;
import view.View;

public class Controller {

    private View vi;
    private Model mo;

    public Controller(View vi, Model mo) {
        this.vi = vi;
        this.mo = mo;
        vi.setController(this);
        mo.setController(this);
    }

    public void setBossName(String bossName) {        
            mo.setBossName(bossName);
        
    }

    public void setWorkerName(String workerName) {
 
            mo.setWorkerName(workerName);
        
    }

    public void getEmployees(){
        mo.getEmployees();
    }

}
