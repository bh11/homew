
package employeemvc;

import controller.Controller;
import model.Model;
import view.ConsoleView;
import view.SwingView;
import view.View;

public class App {

    public static void main(String[] args) {
        
        Model m = new Model();
        View v = new ConsoleView();
        Controller c = new Controller(v, m);
        v.start();
      
    }

}
