package model;

import controller.Controller;
import employee.EmployeeStore;

public class Model {

    private Controller controller;
    
    private EmployeeStore empStore = new EmployeeStore();

    public void setController(Controller controller) {
        this.controller = controller;
    }

    public String getEmployees() {
        return empStore.printEmployees();
    }

    public void setBossName(String bossName) {
        empStore.createBoss(bossName);
    }

    public void setWorkerName(String workerName) {
        empStore.createWorker(workerName);
    }

}
