package bh11_1027_hw5;


public class Bh11_1027_hw5 {

static int count (String str , char c){
    int count = 0;
    for (int i = 0; i < str.length(); i++) {
        if (str.charAt(i) == c) {
            count++;
        }
    }
return count;
}
    public static void main(String[] args) {
        /*Írj olyan metódust, ami paraméterül kap egy stringet és egy karaktert, majd visszatér egy számmal.
       A visszatérési érték azt jelenti, hogy az adott karakter hányszor fordul elő a tömbben.
*/
        System.out.println(count("Megszentségteleníthetetlenségeskedéseitekért", 'e'));

    }

}
