package bh11_1027_hw2;

import java.util.Scanner;


public class Bh11_1027_hw2 {


    public static void main(String[] args) {
/*Olvassunk be egész számokat 0 végjelig egy maximum 100 elemű tömbbe (a tömböt 100 eleműre
deklaráljuk, de csak az elejéből használjunk annyi elemet, amennyit a felhasználó a nulla végjelig beír).
- Írjuk ki a számokat a beolvasás sorrendjében.
- Írjuk ki az elemek közül a legkisebbet és a legnagyobbat, tömbindexükkel együtt.
- Írjuk ki az elemeket fordított sorrendben.
*/

    Scanner sc =new Scanner(System.in);
    
    int[] nums = new int[100];
    int maxIndex =0 , minIndex = maxIndex;
    int max = nums[0];
    int min = Integer.MAX_VALUE;
    int zeroIndex=0;
    boolean isZero = false;
        do {
            System.out.println("Adj meg számokat 0 végjelig: ");
            for (int i = 0; !isZero; i++) {
                int input = sc.nextInt();
                nums[i] = input;
                if (input == 0) {
                    isZero = true;
                    zeroIndex = i;
                }
                
                }
                
        } while (!isZero);
        
            
        for (int i = 0; i < zeroIndex; i++) {
            if (nums[i] != 0 && nums[i] > max) {
                max = nums[i];
                maxIndex = i;
            }
        }
        for (int i = 0; i < zeroIndex; i++) {
            if (nums[i] != 0 && nums[i] < min) {
                min = nums[i];
                minIndex = i;
            }
        }
        System.out.printf("A legnagyobb elem : %d, indexe: %d\n" , max , maxIndex);
        System.out.printf("A legkisebb elem : %d, indexe: %d\n" , min , minIndex);
        System.out.println("A tömb elemei fordítva: ");
        for (int i = zeroIndex - 1; i >= 0; i--) {
            System.out.print(nums[i] + " "); 
    }
       
    }
}
