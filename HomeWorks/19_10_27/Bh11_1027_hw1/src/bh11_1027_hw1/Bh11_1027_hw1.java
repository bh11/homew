package bh11_1027_hw1;

import java.util.Scanner;


public class Bh11_1027_hw1 {


    public static void main(String[] args) {

    /*    Egy busztársaság szeretné tudni, a napok mely óráiban hány ellenőrt érdemes terepmunkára küldenie.
Ehhez összesítik a bírságok adatait. Egy adat a következőkből áll: „óra perc típus összeg”, ahol a típusnál
h a helyszíni bírság, c pedig a csekk. „9 45 c 6000” jelentése: egy utas 9:45-kor csekket kapott 6 000
forintról. A helyszíni bírságokat az ellenőrök begyűjtik; a csekkes bírságoknak átlagosan 80%-át tudják
behajtani. (Vagyis egy 6 000-es csekk a társaság számára csak 4 800-at ér.) Az adatsor végén 0 0 x 0
szerepel.
Olvassa be a C programod ezeket az adatokat! Készítsen kimutatást arról, hogy mely napszakban mennyi
a pótdíjakból a bevétel! Példakimenet:
16:00-16:59, 14800 Ft
17:00-17:59, 12000 Ft
*/
    
    Scanner sc = new Scanner(System.in);
    int hour = -1, min = hour ;
    int fine = 0;
    String type = "";
    String endType="x";
    int[] fineAmount = new int[24];
    
        do {
            System.out.println("Add meg az időt, a bírág típusát és összegét: ");
            hour = sc.nextInt();
            min = sc.nextInt();
            type = sc.next();
           
            fine = sc.nextInt();
            
            if (type.equalsIgnoreCase("c")) {
                int cfine = (fine *80) /100;
               fineAmount[hour] = fineAmount[hour] + cfine;
            }else{
            fineAmount[hour] = fineAmount[hour] + fine;
            
            }
            
            
        } while (hour != 0 && min != 0 && !type.equals(endType) && fine != 0);
        
    for (int i = 0; i < fineAmount.length; i++) {
                  if (fineAmount[i] !=0) {
                      System.out.printf("%d:00 - %d:59, %d Ft\n" , i , i , fineAmount[i]);
                  }
       
            
        
    
    }
    }
}
