package bh11_1027_hw3;

import java.util.Scanner;


public class Bh11_1027_hw3 {


    public static void main(String[] args) {
        
       /* Olvassunk be egész számokat egy 20 elemű tömbbe, majd kérjünk be egy egész számot. Keressük meg a
tömbben az első ilyen egész számot, majd írjuk ki a tömbindexét. Ha a tömbben nincs ilyen szám, írjuk
ki, hogy a beolvasott szám nincs a tömbben.
*/
       Scanner sc = new Scanner (System.in);
       int[] nums = new int[20];
       int i=0;
       
       boolean isNum= false;
        do {
            System.out.println("Adj meg egész számokat a tömbe: ");
            if (sc.hasNextInt()) {
                
            
            for (i = 0; i < nums.length; i++) {
                int input = sc.nextInt();
                nums[i] = input;
                
            }
            isNum=true;
            }else{
            sc.next();
            i--;
            }
        } while (!isNum);

        System.out.println("Adj meg még egy egész számot: ");
        int newInput = sc.nextInt();

        boolean c = false;
        for (int j = 0; j < nums.length; j++) {
            if (nums[j] == newInput) {
                System.out.println("A szám indexe: " + j);
                c = true;
            }
        }

        if (!c) {
            System.out.println("A beolvasott szám nincs a tömbben");
        }
    }

}
