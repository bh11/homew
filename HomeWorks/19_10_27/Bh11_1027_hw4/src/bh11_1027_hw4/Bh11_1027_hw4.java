package bh11_1027_hw4;

import java.util.Scanner;


public class Bh11_1027_hw4 {


    public static void main(String[] args) {
       /* Állítsunk elő egy 50 elemű tömböt véletlen egész számokból (0-tól 9-ig terjedő számok legyenek).
- Írjuk ki a kigenerált tömböt a képernyőre.
- Számítsuk ki az elemek összegét és számtani középértékét.
- Olvassunk be egy 0 és 9 közötti egész számot, majd határozzuk meg, hogy a tömbben ez a szám
hányszor fordul elő.
*/
       Scanner sc = new Scanner (System.in);
       int[] nums = new int[50];
       double avg = 0;
       int count = 0;
        for (int i = 0; i < nums.length; i++) {
            nums[i] = (int)(Math.random()*10);
            
        }
        System.out.println("A generált tömb: ");
        for (int i : nums) {
            
            System.out.print(i + " ");
        }
        int sum=0;
        for (int i = 0; i < nums.length; i++) {
            sum += nums[i];
            avg = (double)sum/50.0;
        }
        System.out.printf("\nAz elemek összegét és számtani középértéke: %.2f\n" , avg);
        
        System.out.println("Adj meg egy 0 és 9 közötti egész számot: ");
        int num = 0;
        if (sc.hasNextInt()) {
            num = sc.nextInt();
            if (num > 9 || num < 0) {
                sc.next();
            }
        }else{
        sc.next();
        }
        for (int i = 0; i < nums.length; i++) {
            if (nums[i] == num) {
                count++;
            }
        }
        System.out.printf("Az adott szám %dx fordul elő a tömbben\n" , count);
    }
   

}
