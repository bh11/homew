/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.log.junite5example;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

/**
 *
 * @author momate
 */
class CalculatorTest {
    
    @BeforeAll
    static void beforeAllMethod(){
        System.out.println("BeforeAllMethod");
    }
    
    @BeforeEach
    void runBeforeAnyMethod(){
        System.out.println("Run before any method");
        
    }
    

    @Test
    void testAdd() {
        //Given  - azok az adatok amik a test bemenő paraméterei, teszt előkészítése
        int op1 = 10;
        int op2 = 15;
        Calculator underTest = new Calculator(op1, op2);
        //When - metódus hívás, az itteni eredményt fogjuk tetsztelni
        int result = underTest.add();
        //Then 
        Assertions.assertEquals(op1 + op2, result, () -> "Wrong addition");

    }

    @Test
    void testSubstract() {
        //Given
        int op1 = 10;
        int op2 = 15;
        Calculator underTest = new Calculator(op1, op2);

        //When
        int result = underTest.substract();

        //Then // azt kaptuk amit akartunk?
        Assertions.assertEquals(op1 - op2, result, () -> "Wrong result");
    }
    
    @Test
    void testMultiply(){
        //Given
        int op1 = 10;
        int op2 = 15;
        Calculator underTest = new Calculator(op1, op2);
        //When
        int result = underTest.multiply();
        //Then
        Assertions.assertEquals(op1 * op2, result, ()-> "Wrong result");
    }
    
    @Test
    void testDevide(){
    //Given
    int op1 = 10;
    int op2 = 15;
    Calculator underTest = new Calculator(op1, op2);
    //When
    int result = underTest.devide();
    //Then
    Assertions.assertEquals(op1 / op2, result, () -> "Wrong result");
    }



}
