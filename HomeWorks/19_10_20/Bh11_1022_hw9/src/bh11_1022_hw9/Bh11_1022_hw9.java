package bh11_1022_hw9;

import java.util.Scanner;


public class Bh11_1022_hw9 {


    public static void main(String[] args) {
        
        //9. A felhasználótól kérj be 2 egész számot 10 és 500 között. Határozd meg a két pozitív egész szám legnagyobb közös osztóját!

        
        Scanner sc = new Scanner (System.in);
        
        boolean isNum = false;
        
        int result=0;
        
        int number1 =0;
          do{
          System.out.println("Adj megy egy egész számot 10 és 500 között: ");
          if(sc.hasNextInt()){
                number1 = sc.nextInt();
                isNum = !(number1<10 || number1 > 500);
          }else{
          sc.next();
          }
      
          }while(!isNum);
          
          
          int number2 =0;
          do{
          System.out.println("Adj megy egy egész számot 10 és 500 között: ");
          if(sc.hasNextInt()){
              number2 = sc.nextInt();
              isNum = !(number2<10 || number2>500);
          }else{
          sc.next();
          }
      
          }while(!isNum);
          
          
          for(int i = 1 ; i<= number1 && i<=number2; i++){
          if(number1 % i == 0 && number2 % i == 0 ){
          result = i;         
          }
          }
          System.out.println("A két szám legnagyobb közös osztója: " +result);
    }

}
