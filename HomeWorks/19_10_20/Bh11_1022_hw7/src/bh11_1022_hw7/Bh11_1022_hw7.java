package bh11_1022_hw7;

import java.util.Scanner;


public class Bh11_1022_hw7 {


    public static void main(String[] args) {
        
        //7. Kérj be egy számot a felhasználótól. Ha 7-tel osztható, írd ki, hogy "kismalac". Ezt a feladatot oldd meg if és switch felhasználásával is.
        Scanner sc = new Scanner (System.in);
        
        boolean isNum = false;
        int number;
        
        //if-else
        
        do{
            System.out.println("Adj meg egy számot: ");
            if(sc.hasNextInt()){
                number = sc.nextInt();
                if(number %7 == 0){
                    System.out.println("Kismalac!");
                    isNum=true;
                }else{
                    isNum=false;
                }
            }else{
                sc.next();
            }         
                
        }while(!isNum);
        
        //switch
                do{
                System.out.println("Adj meg egy számot: ");
                if(sc.hasNextInt()){
                    number = sc.nextInt();
                    switch(number % 7){
                        case 0: System.out.println("Ksimalac"); isNum=true;
                     }
                }else{
                sc.next();
                }         
                
                }while(!isNum);

    }

}
