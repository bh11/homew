package bh11_1022_hw5;

import java.util.Scanner;

public class Bh11_1022_hw5 {


    public static void main(String[] args) {

        //5. Írj programot, mely addig olvas be számokat a billentyűzetről, ameddig azok kisebbek, mint tíz. Írd ki ezek után a beolvasott számok összegét!
        
        Scanner sc = new Scanner (System.in);
        boolean isNum= false;
        int result=0;
     
        do{
            System.out.println("Adj megy egy számot: ");
            if(sc.hasNextInt()){
                int number = sc.nextInt();
                isNum = number <= 10;
                if(number <=10){
                result += number;
                }
            }else{
               sc.next();
            }
            
        }while(isNum);
        
        System.out.println("A beírt számok összege: " +result);
    }

}
