package bh11_1022_hw6;


public class Bh11_1022_hw6 {


    public static void main(String[] args) {
        
        //6. Írd ki a páros számokat 55 és 88 között. Oldd meg ezt a feladatot while és for felhasználásával is.
        System.out.println("Számok while-al: ");
        int i = 55;
        while(i<=88){
        if(i % 2==0){
            System.out.println(i);
        }        
        i++;
        }
        System.out.println("Számok for-al: ");
        
        for(int j =55; j<=88;j++){
        if(j % 2 ==0){
            System.out.println(j);
        }
        }

    }

}
