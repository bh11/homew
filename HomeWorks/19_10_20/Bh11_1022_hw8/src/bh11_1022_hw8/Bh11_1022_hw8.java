package bh11_1022_hw8;

import java.util.Scanner;


public class Bh11_1022_hw8 {


    public static void main(String[] args) {
        
      /*  8. Olvass be 2 számot. Határzod meg, hogy a 2 szám által meghatározott koordináta melyik síknegyedben van.
    X≥0 és Y≥0 → Síknegyed=1
    X≥0 és Y<0 → Síknegyed=4
    X<0 és Y≥0 → Síknegyed=2
    X<0 és Y<0 → Síknegyed=3        
*/
       
      Scanner sc = new Scanner (System.in);
      boolean isNum = false;
      
      int number1 =0;
      do{
          System.out.println("Add meg az első számot: ");
          if(sc.hasNextInt()){
          number1 = sc.nextInt();
          isNum = true;
          }else{
          sc.next();
          }
      
      }while(!isNum);
      
            int number2 =0;
      do{
          System.out.println("Add meg az első számot: ");
          if(sc.hasNextInt()){
          number2 = sc.nextInt();
          isNum = true;
          }else{
          sc.next();
          }
      
      }while(!isNum);
      
       if(number1 >= 0 && number2 >=0){
          System.out.println("1. síknegyed");
      }else if (number1 >= 0 && number2 < 0){
          System.out.println("2. síknegyed");
      }else if (number1 < 0 && number2 >=0){
        System.out.println("3. síknegyed");
      }else if (number1 <=0 && number2 <= 0){
      System.out.println("4. síknegyed");
      }
      
    }

}
