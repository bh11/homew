package bh11_1022_hw4;

import java.util.Scanner;


public class Bh11_1022_hw4 {


    public static void main(String[] args) {

        //4. Kérj be 2 számot a felhasználótól. A kisebbtől a nagyobbig írd ki a 11-re végződő számokat!
        
        Scanner sc = new Scanner (System.in);
        
       boolean isNum = false;
       
       int number1 = -1;
       do{
           System.out.println("Adj meg egy számot: ");
           if(sc.hasNextInt()){
           number1 = sc.nextInt();
           isNum = true;
           }else{
           sc.next();
           }
       }while(!isNum);
       
       
              int number2= -1;
       do{
           System.out.println("Adj meg egy számot: ");
           if(sc.hasNextInt()){
           number2 = sc.nextInt();
           isNum = true;
           }else{
           sc.next();
           }
       }while(!isNum);
       
       
       int from =Math.min(number1, number2);
       int to =Math.max(number1, number2);
       
       
       for(int i = from ; i<=to; i++){
       if((i % 100==11) || i == 11){
           System.out.println(i);
       }
       }
    }

}
