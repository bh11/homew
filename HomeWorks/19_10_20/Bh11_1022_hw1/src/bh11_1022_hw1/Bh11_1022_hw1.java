package bh11_1022_hw1;

import java.util.Scanner;


public class Bh11_1022_hw1 {


    public static void main(String[] args) {
        
        //Kérj be egy számot, írd ki a páros osztóit!
        
        int number;
        boolean isNum = false;
        Scanner sc = new Scanner (System.in);
        do{
        System.out.println("Adj meg egy számot: ");
        if(sc.hasNextInt()){
        number = sc.nextInt();
        isNum = true;
        
        if(number%2==0){
         System.out.println("A "+ number +" páros osztója: ");
        for (int i = 1; i <= number; i++){
        if(number % i == 0 && i % 2 == 0){
           
            System.out.println(i);
           
        }
        }
        }else{
            System.out.println("A "+ number +"-nek nincs páros osztója");
        }
        
        }else{
        sc.next();
        }
        }while(!isNum);

    }

}
