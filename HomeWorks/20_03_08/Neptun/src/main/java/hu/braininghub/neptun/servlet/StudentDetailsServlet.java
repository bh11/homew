/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.neptun.servlet;

import hu.braininghub.neptun.dto.StudentDto;
import hu.braininghub.neptun.service.Neptun;
import java.io.IOException;
import java.util.logging.Logger;
import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author momate
 */
@WebServlet(name = "StudentDetailsServlet", urlPatterns = "/details")
public class StudentDetailsServlet extends HttpServlet {
    
    @Inject
    Neptun service;

    @Override
    public void doGet(HttpServletRequest req, HttpServletResponse resp)
            throws IOException, ServletException {

        int id = Integer.parseInt(req.getParameter("id"));
       
        StudentDto stud = service.getStudetnById(id);
        Logger.getLogger(stud.getName());
         req.setAttribute("stud", stud);
        req.getRequestDispatcher("/WEB-INF/studentDetail.jsp")
                .forward(req, resp);
       
    }

}
