/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.neptun.repository;

import hu.braininghub.neptun.repository.entity.Student;
import java.util.Optional;
import javax.ejb.LocalBean;
import javax.ejb.Singleton;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;

/**
 *
 * @author momate
 */
@LocalBean
@Singleton
public class StudentDao implements CrudRepository<Student, Integer> {

    @PersistenceContext
    private EntityManager em;

    @Override
    public Iterable<Student> findAll() {
        return em.createQuery("SELECT s FROM Student s")
                .getResultList();
    }

    @Override
    public void deleteById(Integer id) {
        Student s = em.find(Student.class, id);
        if (s != null) {
            em.remove(s);
        }
    }

    @Override
    public void update(Student entity) {
        em.merge(entity);
    }

    @Override
    public Optional<Student> findById(Integer id) {
        try {
            Student s = (Student) em.createQuery("SELECT s FROM Student s WHERE s.id=:id")
                    .setParameter("id", id).getSingleResult();

            return Optional.of(s);

        } catch (NoResultException e) {
            return Optional.empty();
        }
    }

    @Override
    public long count() {
        return em.createQuery("SELECT s FROM Student s")
                .getResultList().size();
    }

    @Override
    public void save(Student enntity) {
        em.persist(enntity);
    }

}
