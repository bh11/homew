/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.neptun.repository;

import java.util.Optional;


/**
 *
 * @author momate
 */
public interface CrudRepository<Entity, ID> {
    
    Iterable<Entity> findAll();
    void deleteById(ID id);
    void update(Entity entity);
    void save(Entity enntity);
    Optional<Entity> findById(ID id);
    long count();
}
