/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.neptun.mapper;

import hu.braininghub.neptun.dto.StudentDto;
import hu.braininghub.neptun.dto.SubjectDto;
import hu.braininghub.neptun.repository.entity.Student;
import hu.braininghub.neptun.repository.entity.Subject;
import java.util.ArrayList;
import javax.ejb.LocalBean;
import javax.ejb.Singleton;
import javax.inject.Inject;

/**
 *
 * @author momate
 */
@LocalBean
@Singleton
public class StudentMapper implements Mapper<Student, StudentDto> {

    @Inject
    private SubjectMapper subjectMapper;

    @Override
    public Student toEntity(StudentDto dto) {
        Student s = new Student();
        s.setId(dto.getId());
        s.setName(dto.getName());
        s.setNeptunCode(dto.getNeptunCode());
        s.setDateOfBirth(dto.getDateOfBirth());

        s.setSubjects(new ArrayList<>());

        dto.getSubjects().forEach((subjectDto) -> {
            s.getSubjects().add(subjectMapper.toEntity(subjectDto));
        });

        return s;
    }

    @Override
    public StudentDto toDto(Student entity) {
        StudentDto s = new StudentDto();
        s.setId(entity.getId());
        s.setName(entity.getName());
        s.setNeptunCode(entity.getNeptunCode());
        s.setDateOfBirth(entity.getDateOfBirth());

        s.setSubjects(new ArrayList<>());

        entity.getSubjects().forEach((subject) -> {
            s.getSubjects().add(subjectMapper.toDto(subject));
        });

        return s;

    }

}
