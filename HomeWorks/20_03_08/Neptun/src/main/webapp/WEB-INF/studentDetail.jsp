<%-- 
    Document   : studentDetail
    Created on : 2020.03.01., 14:29:43
    Author     : momate
--%>

<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
        <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

        <title>JSP Page</title>
    </head>
    <body>
        <h1>Student: </h1>
        <h1>Details:</h1>

        <table class="table table-striped table-dark">
            <thead>
                <tr>
                    <th>Id:</th>
                    <th>Name:</th>
                    <th>Neptun code:</th>
                    <th>Date of birth:</th>
                    <th>Subjects:</th>
                </tr>
            </thead>
            <tbody>
                
                <tr>
                    <td><c:out value="${stud.id}"/> </td>
                    <td><c:out value="${stud.name}"/> </td>
                    <td><c:out value="${stud.neptunCode}"/> </td>
                    <td><c:out value="${stud.dateOfBirth}"/> </td>
                    <c:forEach var="s" items="${stud.subjects}"/>
                    <td><c:out value="${s.name}"/></td>
                    
                </tr>
              
            </tbody>
        </table>
    </body>
</html>
