package hu.braininghub.hr;

import java.awt.BorderLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

public class SwingView extends JFrame {

    HrDao dao = new HrDao();

    JTextArea result;
    JScrollPane scroll;
    JButton button1 = new JButton("1");
    JButton button2 = new JButton("2");
    JButton button3 = new JButton("3");
    JButton button4 = new JButton("4");
    JButton button5 = new JButton("5");
    JButton button6 = new JButton("6");

    private JPanel buildNorth() {
        JPanel jp = new JPanel();
        jp.add(button1);
        jp.add(button2);
        jp.add(button3);
        jp.add(button4);
        jp.add(button5);
        jp.add(button6);

        makeListeners();

        return jp;
    }

    private void makeListeners() {

        button1.addActionListener(l -> result.setText(dao.findEmployeeWithMaxSalary()));
        button2.addActionListener(l -> result.setText(dao.findDepartment()));
        button3.addActionListener(l -> result.setText(dao.fineEmloyeeWithAboveAverageSalray()));
        button4.addActionListener(l -> result.setText(dao.findEmloyeeAfter1990()));
        button5.addActionListener(l -> result.setText(dao.isThereClerk()));

        button6.addActionListener(l -> result.setText(dao.findEmployeeByIDSafe(popUp())));

    }

    private String popUp() {
        JFrame frame = new JFrame();
        Object id = JOptionPane.showInputDialog(frame, "Give me an ID number: ");

        return id.toString();

    }

    private JPanel buildCenter() {
        result = new JTextArea(50, 50);
        scroll = new JScrollPane(result);
        JPanel jp = new JPanel();
        result.setEditable(false);
        jp.add(result);

        return jp;

    }

    private void contsrtuct() {
        add(buildNorth(), BorderLayout.NORTH);
        add(buildCenter(), BorderLayout.CENTER);

    }

    public void build() {
        contsrtuct();
        // pack();
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setVisible(true);

    }
}
