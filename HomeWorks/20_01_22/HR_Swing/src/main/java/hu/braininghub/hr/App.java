package hu.braininghub.hr;

public class App {

    private static final String STOP = "exit";

    public static void main(String[] args) {
        HrDao dao = new HrDao();

        SwingView view = new SwingView();
        view.build();

    }

}
