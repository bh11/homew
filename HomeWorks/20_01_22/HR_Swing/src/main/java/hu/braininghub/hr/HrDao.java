package hu.braininghub.hr;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

public class HrDao {

    private static final String URL = "jdbc:mysql://localhost:3306/hr?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC";
    private static final String USER = "root";
    private static final String PW = "root";

    public String findEmployeeWithMaxSalary() {
        String sql = "Select * from employees where salary =(Select max(salary) from employees)";

        String result = "";

        try (
                Connection conn = DriverManager.getConnection(URL, USER, PW);
                Statement stm = conn.createStatement()) {

            ResultSet rs = stm.executeQuery(sql);

            while (rs.next()) {
                result += rs.getString("first_name") + " " + rs.getString("last_name") + "\n";
            }

        } catch (SQLException ex) {
            System.out.println(ex);
        }

        return result;
    }

    public String findDepartment() {
        String sql = "Select department_name from departments where department_id =  (Select department_id FROM employees e GROUP BY department_id ORDER BY count(*) DESC LIMIT 1)";

        String result = "";

        try (
                Connection conn = DriverManager.getConnection(URL, USER, PW);
                Statement stm = conn.createStatement()) {

            ResultSet rs = stm.executeQuery(sql);

            while (rs.next()) {

                result += rs.getString("department_name") + "\n";
            }

        } catch (SQLException ex) {
            System.out.println(ex);
        }
        return result;
    }

    public String fineEmloyeeWithAboveAverageSalray() {
        String sql = "Select * from employees where salary > (select avg(salary) from employees)";

        String result = "";

        try (
                Connection conn = DriverManager.getConnection(URL, USER, PW);
                Statement stm = conn.createStatement()) {

            ResultSet rs = stm.executeQuery(sql);

            while (rs.next()) {
                result += rs.getString("first_name") + " " + rs.getString("last_name") + "\n";
            }

        } catch (SQLException ex) {
            System.out.println(ex);
        }
        return result;
    }

    public String findEmloyeeAfter1990() {
        String sql = "Select * from employees where hire_date > '1990.01.01'";

        String result = "";

        try (
                Connection conn = DriverManager.getConnection(URL, USER, PW);
                Statement stm = conn.createStatement()) {

            ResultSet rs = stm.executeQuery(sql);

            while (rs.next()) {
                result += rs.getString("first_name") + " " + rs.getString("last_name") + "\n";
            }

        } catch (SQLException ex) {
            System.out.println(ex);
        }
        return result;

    }

    public String isThereClerk() {

        String sql = "SELECT DISTINCT j.job_title title FROM hr.jobs j WHERE j.job_title LIKE '%Clerk%' ORDER BY title";

        String result = "";

        try (
                Connection conn = DriverManager.getConnection(URL, USER, PW);
                Statement stm = conn.createStatement()) {

            ResultSet rs = stm.executeQuery(sql);

            while (rs.next()) {

                result += rs.getString("title") + "\n";
            }

        } catch (SQLException ex) {
            System.out.println(ex);
        }
        return result;
    }

    public String findEmployeeByIDSafe(String id) {
        String sql = "Select * from employees e where e.employee_id = ?";

        String result = "";
        try (
                Connection conn = DriverManager.getConnection(URL, USER, PW);
                PreparedStatement stm = conn.prepareStatement(sql);) {

            stm.setInt(1, Integer.parseInt(id));
            ResultSet rs = stm.executeQuery();

            while (rs.next()) {
                result += rs.getString("first_name") + " " + rs.getString("last_name") + "\n";
            }

        } catch (SQLException ex) {
            System.out.println(ex);
        }
        return result;

    }

    public void findEmployeeByFirstName(String name) {
        String sql = "SELECT * \n"
                + "FROM employees e \n"
                + "WHERE e.first_name = ?";
        try (Connection connection = DriverManager.getConnection(URL, USER, PW);
                PreparedStatement stm = connection.prepareStatement(sql)) {
            stm.setString(1, name);
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                System.out.println(rs.getString("first_name") + " " + rs.getString("last_name"));
            }
        } catch (SQLException ex) {
            Logger.getLogger(HrDao.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
