
package filefilterswing;

import controller.Controller;
import controller.FileController;
import model.FileStore;
import model.Model;
import view.SwingView;
import view.View;

public class FileFilterSwing {

    public static void main(String[] args) {
        
        View v = new SwingView();
        Model m = new FileStore();
        Controller c = new FileController(v, m);
        v.start();

      
    }

}
