package collector;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class FileCollector {

    public List<File> collect(File path){
        if (path == null || !path.isDirectory()) {
            return new ArrayList<>();
        }
        
        return Arrays.stream(path.listFiles())
                .filter(File::isFile)
                .collect(Collectors.toList());
    }

}
