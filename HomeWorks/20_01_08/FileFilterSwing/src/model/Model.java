package model;

import controller.ModelController;
import java.io.File;
import java.util.List;

/**
 *
 * @author momate
 */
public interface Model {
    
    void setController(ModelController c);
    List<File> getFiles();
    void setFiles(List<File> files);
}
