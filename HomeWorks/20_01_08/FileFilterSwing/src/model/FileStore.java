package model;


import controller.ModelController;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class FileStore implements Model{
    
    private ModelController controller;
    private List<File> files = new ArrayList<>();
 

    @Override
    public java.util.List<File> getFiles() {
        return files;
    }

    @Override
    public void setController(ModelController controller) {
       this.controller = controller;
    }

    @Override
    public void setFiles(List<File> files) {
        this.files = files;
        controller.notifyView();
    }

   

}
