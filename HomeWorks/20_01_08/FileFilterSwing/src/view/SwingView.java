package view;

import controller.ViewController;
import java.awt.BorderLayout;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.List;
import java.util.stream.Collectors;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;

public class SwingView extends JFrame implements View {
    
    private static final String FILE = "/home/momate/Asztal/Things/save.ser";

    private ViewController controller;

    private JTextArea jTextArea;
    private JButton jButton;
    private JTextField jTextField;

    @Override
    public void setController(ViewController controller) {
        this.controller = controller;
    }

    @Override
    public void update(List<File> files) {
        jTextArea.setText(files.stream()
                .map(File::getPath)
                .collect(Collectors.joining("\n"))
        );
    }

    @Override
    public void start() {

        construct();

        setDefaultCloseOperation(EXIT_ON_CLOSE);
        pack();
        setVisible(true);

    }

    private void construct() {
        jTextArea = new JTextArea(50, 50);
        jButton = new JButton("GO");
        jTextField = new JTextField(30);

        JPanel p1 = new JPanel();
        p1.add(jTextField);
        p1.add(jButton);

        JPanel p2 = new JPanel();
        p2.add(jTextArea);
        jButton.addActionListener(e -> controller
                .handleGoButton(jTextField.getText()));

        add(p1, BorderLayout.NORTH);
        add(p2, BorderLayout.CENTER);
        closeOperation();

    }

    public void closeOperation() {
        addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                save();
                System.exit(0);
            }
        });
    }
    
    private void save(){
        try(ObjectOutputStream ou = new ObjectOutputStream(new FileOutputStream(FILE))){
            ou.writeObject(jTextArea.getText());
        }catch(IOException ex){
            System.out.println(ex);
        }
            
    
    }

}
