package view;


import controller.ViewController;
import java.io.File;
import java.util.List;

/**
 *
 * @author momate
 */
public interface View {
    
    void setController(ViewController con);
    
   void update(List<File> files);
   
   void start();
    
}
