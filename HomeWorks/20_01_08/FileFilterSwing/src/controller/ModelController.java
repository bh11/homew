package controller;

/**
 *
 * @author momate
 */
public interface ModelController extends Controller {
    
    void notifyView(); // model
}
