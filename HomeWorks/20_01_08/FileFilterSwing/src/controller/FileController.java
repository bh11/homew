package controller;

import collector.FileCollector;
import filter.FilterAggregator;
import java.io.File;
import java.util.Collections;
import java.util.List;
import model.Model;
import view.View;

public class FileController implements ModelController, ViewController {
    
    private final View view;
    private final Model model;
    
    private final FileCollector fileCollector = new FileCollector();
    private final FilterAggregator filterAggregator = new FilterAggregator();
    
    public FileController(View view, Model model) {
        this.view = view;
        this.model = model;
        
        model.setController(this);
        view.setController(this);
    }
    @Override
    public void notifyView() {
        List<File> files = model.getFiles();
        view.update(Collections.unmodifiableList(files));
        
    }
    @Override
    public void handleGoButton(String path) {
        List<File> files = fileCollector.collect(new File(path));
        List<File> filteredFiles = filterAggregator.filter(files);
        
        model.setFiles(filteredFiles);
    }
    
}