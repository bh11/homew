package filter;

import handler.Filter;
import handler.ParenntDirectroyContainsALetter;
import handler.SizeLimited2MbFilter;
import handler.SmallLetterContainedFilter;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class FilterAggregator {
    private final List<Filter> filters = new ArrayList<>();

    public FilterAggregator() {
        filters.add(new SmallLetterContainedFilter());
        filters.add(new SizeLimited2MbFilter());
        filters.add(new ParenntDirectroyContainsALetter());
    }

    public List<File> filter(List<File> files) {

        return files.stream()
                .filter(this::allFilterMatch) // f -> allFilterMatched(f)
                .collect(Collectors.toList());

    }

    private boolean allFilterMatch(File f) {
        for (Filter filter : collectRelevantFilters(f)) {
            if (!filter.filter(f)) {
                return false;
            }
        }
        return true;
    }

    private List<Filter> collectRelevantFilters(File file) {
        return filters.stream().filter(f -> f.test(file)).collect(Collectors.toList());
    }
}
