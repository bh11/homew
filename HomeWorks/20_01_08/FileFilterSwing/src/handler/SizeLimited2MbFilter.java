package handler;

import java.io.File;

public class SizeLimited2MbFilter implements Filter {

    @Override
    public boolean filter(File file) {
       return convertToMegabyteFromByte(file.length()) < 2D;
    }
    
    private double convertToMegabyteFromByte(long value){
        return value / 1024D / 1024D;
    }

    @Override
    public boolean test(File file) {
        return true;
    }

}
