package handler;

import java.io.File;



/**
 *
 * @author momate
 */
public interface Filter {
    
    boolean filter(File file);
    
    boolean test(File file);
      
}
