package handler;

import java.io.File;

public class AgeLimitedFilter implements Filter {
    
    private static final String USER_HOME = "home"; 

    @Override
    public boolean filter(File file) {
        return convertMilisecToDay(file.lastModified()) < 2D;

                
    }

    @Override
    public boolean test(File file) {
        return file.getPath().contains(USER_HOME);
    }
    
    private double convertMilisecToDay(long value){
        return value / 1000D / 3600D / 24D;
    
    }
    
    

}
