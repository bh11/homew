package handler;

import java.io.File;

public class SmallLetterContainedFilter implements Filter {

    @Override
    public boolean filter(File file) {
        String name = file.getName();
        for (int i = 0; i < name.length(); i++) {
            if (isSmallLetter(name.charAt(i))) {
                return true;

            }
        }

        return false;
    }

    private boolean isSmallLetter(char c) {
        return c >= 'a' && c <= 'z';
    }

    @Override
    public boolean test(File file) {
        return true;
    }

}
