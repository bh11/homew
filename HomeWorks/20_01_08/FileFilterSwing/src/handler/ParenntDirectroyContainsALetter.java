package handler;

import java.io.File;

public class ParenntDirectroyContainsALetter implements Filter {
    
    private static final String letter = "A";

    @Override
    public boolean filter(File file) {
       return file.getParent().contains(letter);
    }

    @Override
    public boolean test(File file) {
        return true;
    }

}
