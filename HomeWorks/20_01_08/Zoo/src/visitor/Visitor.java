package visitor;

import animal.AbstractAnimal;

/**
 *
 * @author momate
 */
public interface Visitor {
    
    void act(AbstractAnimal animal);
    void feed(AbstractAnimal animal);
    
}
