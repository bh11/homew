package visitor;

import animal.AbstractAnimal;
import zoo.shop.Food;

public class Adult implements Visitor {

    @Override
    public void act(AbstractAnimal animal) {
        animal.setVisitors(animal.getVisitors() + 1);
    }

    @Override
    public void feed(AbstractAnimal animal) {
        animal.eat(new Food());
    }

}
