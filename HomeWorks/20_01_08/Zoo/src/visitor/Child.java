package visitor;

import animal.AbstractAnimal;
import animal.HappinessFactor;

public class Child implements Visitor {

    @Override
    public void act(AbstractAnimal animal) {
        animal.setVisitors(animal.getVisitors()+2);
        
        animal.setHunger(animal.getHunger() - 1);
        animal.setHappiness(HappinessFactor.nextLevel(animal.getHappiness()));
    }

    @Override
    public void feed(AbstractAnimal animal) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
