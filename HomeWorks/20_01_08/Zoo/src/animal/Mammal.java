package animal;

import zoo.shop.Food;

public abstract class Mammal extends AbstractAnimal {

    public Mammal(String name, int requiredPlace) {
        super(name, requiredPlace);
    }
    @Override
    public void eat(Food food){
        setHunger(getHunger()+2);
    }

}
