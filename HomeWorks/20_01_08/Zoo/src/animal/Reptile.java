package animal;

import zoo.shop.Food;

public abstract class Reptile extends AbstractAnimal {

    public Reptile(String name, int requiredPlace) {
        super(name, requiredPlace);
    }
    @Override
    public void eat(Food food) {
        setHunger(getHunger() + 1.5);
    }

}
