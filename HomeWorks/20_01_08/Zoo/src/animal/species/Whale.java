package animal.species;

import animal.Mammal;
import animal.feature.Aquatic;

public class Whale extends Mammal implements Aquatic{

    public Whale(String name, int requiredPlace) {
        super(name, requiredPlace);
    }

}
