package animal.species;

import animal.Mammal;
import animal.feature.Flying;
import animal.feature.Terrestrial;

public class Bat extends Mammal implements Terrestrial, Flying{

    public Bat(String name, int requiredPlace) {
        super(name, requiredPlace);
    }

    @Override
    public void fly() {
        System.out.println("I'am flying. " + toString());
    }

}
