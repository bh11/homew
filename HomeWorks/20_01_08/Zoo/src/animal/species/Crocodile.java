package animal.species;

import animal.Reptile;
import animal.feature.Aquatic;
import animal.feature.Terrestrial;

public class Crocodile extends Reptile implements Terrestrial,Aquatic{

    public Crocodile(String name, int requiredPlace) {
        super(name, requiredPlace);
    }

}
