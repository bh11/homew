package animal.species;

import animal.Mammal;
import animal.feature.Terrestrial;

public class Zebra extends Mammal implements Terrestrial{

    public Zebra(String name, int requiredPlace) {
        super(name, requiredPlace);
    }

}
