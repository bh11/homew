package animal;

import java.util.Arrays;
import java.util.Comparator;
import java.util.Optional;

/**
 *
 * @author momate
 */
public enum HappinessFactor {

    SAD(0),
    NEUTRAL(1),
    HAPPY(2);

    private final int order;

    private HappinessFactor(int order) {
        this.order = order;
    }

    public static HappinessFactor nextLevel(HappinessFactor factor) {
        if (factor == HAPPY) {
            return HAPPY;
        }

        Optional<HappinessFactor> nextLevel = Arrays.stream(HappinessFactor.values())
                .sorted((x,y) -> y.order -x.order)
                .filter(f -> f.order < factor.order)
                .findAny();

        return nextLevel.orElse(HAPPY);
    }
}
