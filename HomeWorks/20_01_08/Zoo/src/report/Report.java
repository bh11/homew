package report;

import animal.HappinessFactor;
import zoo.Zoo;

public class Report {
    
    
    Zoo zoo = new Zoo();

    public void unhappyAndHungryAnimals() {
        zoo.getCages().stream()
                .flatMap(m -> m.getAnimels()
                .stream()).filter(c -> c.getHappiness() != HappinessFactor.HAPPY && c.getHunger() < 5D)
                .forEach(g -> System.out.print(g + ", "));
    }
    
    public void childVisitorsByCages(){
        zoo.getCages().stream()
                .mapToInt(c -> c.getAnimels()
                        .stream()
                        .mapToInt(b -> b.getVisitors())
                        .sum())
                .forEach(System.out::println);
    }

}
