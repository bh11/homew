package zoo;

import java.util.List;
import report.Report;
import visitor.Visitor;

public class Zoo {

    private List<Cage> cages;
    private List<Visitor> visitors;
    Report report = new Report();

    public void addCage(Cage cage) {
        cages.add(cage);
    }

    public void addVisitor(Visitor visitor) {
        visitors.add(visitor);
    }

    public void addAnimal() {

    }

    public void removeCage(Cage cage) {
        cages.remove(cage);
    }

    public void removeVisitor(Visitor visitor) {
        visitors.remove(visitor);
    }

    public void removeAnimal() {

    }

    public void report() {
        System.out.println("Unhappy and Hungry annimals: ");

        report.unhappyAndHungryAnimals();
        
        System.out.println("Number of child visitors: ");
        report.childVisitorsByCages();

    }

    public List<Cage> getCages() {
        return cages;
    }

    public List<Visitor> getVisitors() {
        return visitors;
    }

}
