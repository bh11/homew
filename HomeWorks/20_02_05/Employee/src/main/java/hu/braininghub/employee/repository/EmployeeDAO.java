/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.employee.repository;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.annotation.Resource;
import javax.ejb.Singleton;
import javax.sql.DataSource;

/**
 *
 * @author momate
 */
@Singleton  // Annotációtól ez már egy EJB
public class EmployeeDAO {

    @Resource(lookup = "jdbc/_mm")  //payara resource 
    private DataSource ds;

    @PostConstruct  //Reflection api  - mindegy a név
    public void init() {
        System.out.println("It has been initialized");
    }

    @PreDestroy
    public void destroy() {
        System.out.println("Ahhh meghalok");
    }

    public List<String> getNames() throws SQLException {
        List<String> names = new ArrayList<>();

        try (Statement stm = ds.getConnection().createStatement()) {
            String sql = "Select * from employees";

            ResultSet rs = stm.executeQuery(sql);

            while (rs.next()) {
                names.add(rs.getString("first_name"));

            }

        }

      return names;
   }

    public List<String> searchNamesByInput(String str) throws SQLException {
        List<String> searchedNames = new ArrayList<>();

//        try (PreparedStatement pstm = ds.getConnection().prepareStatement("Select * from employees where first_name like '%?%'")) {
//            pstm.setString(1, str);
//
//            
//            ResultSet rs = pstm.executeQuery();
//
//            while (rs.next()) {
//                searchedNames.add(rs.getString(2));
//            }
//
//        }
//
//        return searchedNames;
//    }
        try (Statement stm = ds.getConnection().createStatement()) {
            String sql = "Select * from employees where first_name like '%" + str +"%'";

            ResultSet rs = stm.executeQuery(sql);

            while (rs.next()) {
                searchedNames.add(rs.getString("first_name"));

            }

        }

        return searchedNames;
    }
}
