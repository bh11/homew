
package serilazitinexample;



public class App {
    
    
    public static void main(String[] args) {
       
        
        Employee emp1 = new Employee("Kis Sanyi", 23, 200000, new Department("Corvin", "HR"));
        Employee emp2 = new Employee("Nagy Béla", 56, 120000, new Department("Kis ut","IT"));
        Employee emp3 = new Employee("Ezek Elek", 25, 350000, new Department("Vaci ut","Sales"));
        Employee emp4 = new Employee("Ezek Ila", 25, 350000, new Department("Vaci ut","Sales"));
        
        
       
        EmployeeRepository.addEmployee(emp1);
        EmployeeRepository.addEmployee(emp2);
        EmployeeRepository.addEmployee(emp3);
        EmployeeRepository.addEmployee(emp4);
        
        System.out.println("Print all employee's: ");
        EmployeeRepository.printEmployees();
        
        EmployeeRepository.deleteEmployee(emp2);
        System.out.println("Print updated employee list: ");
        EmployeeRepository.printEmployees();
        System.out.println("Print one employee: ");
        EmployeeRepository.getEmployee("Ezek Elek");
        
        EmployeeRepository.printByDepartments("HR");
        
       
        
    }
      
}

