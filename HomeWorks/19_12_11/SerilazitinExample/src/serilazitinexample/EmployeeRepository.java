package serilazitinexample;

import com.sun.istack.internal.logging.Logger;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;


public class EmployeeRepository{
   private static List<Employee> employees = new ArrayList<>() ;
   private static final String FILENAME = "employee.ser"; 

    public static void addEmployee(Employee emp){
        employees.add(emp);
    }
    
    public static void deleteEmployee(Employee emp){
        employees.remove(emp);
    }
    
    public static void printEmployees(){
        employees.stream()
                .forEach(System.out::println);
    }
    
    public static void getEmployee(String name){
        employees.stream()
                .filter(e-> name.equals(e.getName()))
                .forEach(System.out::println);
    }
    
    public static void printByDepartments(String depart){
        employees.stream()
                .map(c -> c.getDepartment()).filter(b -> depart.equals(b.getName()))
                .forEach(System.out::println);
    }
   
    
    public static void serializeEmployee(Set emp){
        try (FileOutputStream fs = new FileOutputStream(FILENAME)){
            ObjectOutputStream ou = new ObjectOutputStream(fs);
            ou.writeObject(emp);
        } catch (FileNotFoundException ex) {
         
        } catch (IOException ex) {
            
        }
    }
    
    public static LinkedHashSet<Employee> deserializeEmployee(){
        LinkedHashSet<Employee> emp = null;
        try (FileInputStream fs = new FileInputStream(FILENAME)){
            ObjectInputStream oi = new ObjectInputStream(fs);
            emp = (LinkedHashSet<Employee>) oi.readObject();
        } catch (FileNotFoundException ex) {
            
        } catch (IOException | ClassNotFoundException ex) {
           
        }
        return emp;
    }
}

