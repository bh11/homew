/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.pizzas.pizzas;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author momate
 */
public class Orderrr extends HttpServlet {

OrderBase orderBase = new OrderBase();

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
               this.orderBase.addOrder(new Order(request.getParameter("pizzaType"), 
                Integer.parseInt(request.getParameter("size")), 
                request.getParameter("name"), 
                request.getParameter("email"), 
                request.getParameter("address")));
               
               request.getRequestDispatcher("ordering.jsp").forward(request, response);
    }

}
