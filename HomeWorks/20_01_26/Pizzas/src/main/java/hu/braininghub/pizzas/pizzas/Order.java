/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.pizzas.pizzas;

/**
 *
 * @author momate
 */
public class Order {
    
    private String pizzaType;
    private int size;
    private String name;
    private String email;
    private String address;

    public Order(String pizzaType, int size, String name, String email, String address) {
        this.pizzaType = pizzaType;
        this.size = size;
        this.name = name;
        this.email = email;
        this.address = address;
    }

    public String getPizzaType() {
        return pizzaType;
    }

    public void setPizzaType(String pizzaType) {
        this.pizzaType = pizzaType;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
    
    
}
