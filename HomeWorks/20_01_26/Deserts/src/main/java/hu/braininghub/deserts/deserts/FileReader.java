/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.deserts.deserts;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.util.Scanner;

/**
 *
 * @author momate
 */
public class FileReader {
    
    LineParser parser = new LineParser();
    File file = new File("/home/momate/Dokumentumok/BrainingHub/Bh11/2020.01.24/Deserts/deserts.txt");

    public void read() {
        
        try (Scanner sc = new Scanner(file)) {
            while (sc.hasNextLine()) {
                parser.parse(sc.nextLine());
                
            }
            
        } catch (IOException ex) {
            System.out.println(ex);
        }
        
    }
    
}
