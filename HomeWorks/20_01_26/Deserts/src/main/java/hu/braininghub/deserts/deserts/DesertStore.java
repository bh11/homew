/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.deserts.deserts;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author momate
 */
public class DesertStore {
    
    private List<Desert> deserts = new ArrayList<>();

    public List<Desert> getDeserts() {
        return deserts;
    }
    
    public void addDesert(Desert desert){
        deserts.add(desert);
    }
    
}
