package hu.braininghub.deserts.deserts;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author momate
 */
public class LineParser {
    
    
    DesertStore desertStore = new DesertStore();
    
    private static final String DELIMETER = "|";
    
     public void parse(String line){
         String[] data = line.split(DELIMETER);
         desertStore.addDesert(new Desert(data[0] ,Integer.valueOf(data[1])));
    }
}
