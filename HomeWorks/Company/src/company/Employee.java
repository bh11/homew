package company;



public class Employee {
    private String name;
    private String id;
    
    private Bookings[] units = new Bookings[2];
    private int numberOfUnits = 0;
    
    

    public Employee(String names, String systemIds) {
        setName(names);
        setSystemId(systemIds);
        
    }
    
    public Bookings getBooking(int i){
    return units[i];
    }
    
    /**
     *
     * @param ids
     * @param country
     */
    public void addBooking(String ids, String country){
        addBookings(new Bookings(ids, country));
    }

    public void addBookings(Bookings inv) {
        if (numberOfUnits < units.length) {
            units[numberOfUnits] = inv;
            numberOfUnits++;
        }
    }
    
        public void increaseInvoiceAmount() {
        Invoice.amount *= Invoice.inreaseAmount;
    }
    
        public void increaseInvoiceAmounts () {
        for (Invoice emp : Bookings.invoice) {
            this.increaseInvoiceAmount();
        }

    }
        
            public void printAllInvoice() {
        for (int i = 0; i < Bookings.numberOfInvocies; i++) {
            System.out.printf("%s number: $%.1f\n", Bookings.invoice[1].getSerialNumber(), Bookings.invoice[0].getAmount());
        }
    }

    public void decreaseInvoiceAmount() {
        Invoice.amount *= Invoice.decreaseAmount;
    }
    
        public void decreaseInvoiceAmounts () {
        for (Invoice emp : Bookings.invoice) {
            this.decreaseInvoiceAmount();
        }

    }



    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSystemId() {
        return id;
    }

    public void setSystemId(String systemId) {
        this.id = systemId;
    }
}
