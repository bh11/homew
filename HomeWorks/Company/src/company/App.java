
package company;

public class App {

    public static void main(String[] args) {
        
        Employee e1 = new Employee("Zoltan", "kiskece");
        e1.addBooking("1214", "hun");
        e1.getBooking(0).addInvoice("0103", 1000);
        e1.getBooking(0).addInvoice("1212", 233000);
        e1.getBooking(0).addInvoice("12313", 21323113);
        
        e1.increaseInvoiceAmount();
        
        e1.printAllInvoice();
      
      
    }

}
