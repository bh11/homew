package company;

public class Invoice {

    public static double amount;
    private String serialNumber;

    static final double DEFAULT_AMOUNT = 1000.0;

    public static double inreaseAmount = 1.1;
    public static double decreaseAmount = 0.9;

    public Invoice(String serialNumber, double amount) {
        setSerialNumber(serialNumber);
        if (amount == 0) {
            amount = 1000;
        }
        setAmount(amount);
    }

//    public Invoice(String number) {
//        this(number, DEFAULT_AMOUNT);
//
//    }

    public void setAmount(double amount) {
        Invoice.amount = amount;
    }

    public void setSerialNumber(String serialNumber) {
        this.serialNumber = serialNumber;
    }

    public double getAmount() {
        return amount;
    }

    public String getSerialNumber() {
        return serialNumber;
    }

}
