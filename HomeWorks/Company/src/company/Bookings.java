package company;

public class Bookings {

    private String ids;
    private String country;
    public static Invoice[] invoice = new Invoice[10];
    public static int numberOfInvocies = 0;
    
    public Bookings(String ids, String country) {
        setCountry(country);
        setId(ids);
    }
    
    public void addInvoice(String serialNumber, int amount){
        addInvoices(new Invoice(serialNumber, amount));
    
    }
//    
//        public void addInvoice(String serialNumber){
//        addInvoices(new Invoice(serialNumber));
//    
//    }
    
    
    public void addInvoices(Invoice inv) {
        if (numberOfInvocies < invoice.length) {
            invoice[numberOfInvocies] = inv;
            numberOfInvocies++;
        }
        
    }



    public String getIds() {
        return ids;
    }
    
    
    
    public void setId(String ids) {
        this.ids = ids;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public int getNumberOfInvocies() {
        return numberOfInvocies;
    }
    
    

}
//
//    private String id;
//    private String country;
//    private Invoice[] invoice = new Invoice[10];
//    private int numberOfInvocies = 0;
//
//    public Bookings(String id, String country) {
//        setCountry(country);
//        setId(id);
//    }
//
//    public void addInvoice(String number, int amount) {
//        addInvoices(new Invoice(number, amount));
//    }
//    
//        public void addInvoice(String number) {
//        addInvoices(new Invoice(number));
//    }
//
//
//    public void addInvoices(Invoice inv) {
//        if (numberOfInvocies < invoice.length) {
//            invoice[numberOfInvocies] = inv;
//            numberOfInvocies++;
//        }
//    }
//
//    public void printAllInvoice() {
//        for (int i = 0; i < numberOfInvocies; i++) {
//            System.out.printf("%s number: $%.1f\n", invoice[i].getNumber() , invoice[i].getAmount());
//        }
//    }
//

//
//
//
//    public void setId(String id) {
//        this.id = id;
//    }
//
//    public void setCountry(String country) {
//        this.country = country;
//    }
//
//}
