
package bh11_1106_hw4;

public class Bh11_1106_hw4 {
    
    
        static int[][] nums = new int[14][10];
    
        static void randomArr(int[][] nums){
        for (int i = 0; i < nums.length; i++) {
            for (int j = 0; j < nums[0].length; j++) {
                 nums[i][j] = (int)(Math.random()*100);
            }
           
        }
    
    }
        static int[] minInt(int[][] nums){
        int minValue = nums[0][0];
        int[] min = new int[3];
            for (int i = 0; i < nums.length; i++) {
                for (int j = 0; j < nums[i].length-1; j++) {
                    if (nums[i][j] < minValue ) {
                    minValue = nums[i][j];
                    min[0] = i;
                    min[1] = j;
                }
                }
            }
            min[2] = minValue;
            
        return min;
        }
        static void printResult(int[] a){
            System.out.printf("The row is: %d\n" , a[0]) ;
            System.out.printf("The column is: %d\n" , a[1]);
            System.out.printf("The value is: %d\n" , a[2]);

        }

    public static void main(String[] args) {
        /*
        írjunk metódust, ami egy n*m-es kétdimenziós tömböt feltölt véletlenszerű elemekkel valamilyen határok között, majd írjunk egy másik metódust, 
        ami egy n*m-es kétdimenziós tömb legkisebb elemével és annak helyével tér vissza. A visszatérés egy tömb legyen, az alábbi formában [sor, oszlop, érték]
        */
        randomArr(nums);
  
        for (int[] x : nums){
        for (int y : x)
        {
        System.out.print(y + " ");
         }
        System.out.println();
             
    }
         
        printResult(minInt(nums));
        
    }
}
