
package bh11_1106_hw3;

public class Bh11_1106_hw3 {
    

    static int sumNumbers(int num) {
       int res = 0;
       while(num>0) {
           res += num % 10;
           num /= 10;
       }
       return res;
    }

    static int recSumNumbers(int n){
        if (n == 0) {
            return n;
        }
       return n%10 + recSumNumbers(n/10);
    }

    public static void main(String[] args) {
      /*
         - adott szám számjegyeinek összege rekurzív módon
        */
        System.out.println(sumNumbers(1012));
        System.out.println(recSumNumbers(1012));
    }

}
