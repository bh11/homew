
package bh11_1106_hw2;

public class Bh11_1106_hw2 {
    static int[] nums = new int[300];

    static void randomArr(int[] nums){
        for (int i = 0; i < nums.length; i++) {
            nums[i] = (int)(Math.random()*100);
        }
    
    }
    
    static int mostCom(int[] num){
    int number = 0;
    int count = 0;
    
        for (int i = 0; i < num.length; i++) {
            int temp_num = num[i];
            int temp_count = 0;
            for (int j = 0; j < num.length; j++) {
                if (num[j] == temp_num) {
                    temp_count++;
                    if (temp_count > count) {
                        number = temp_num;
                        count = temp_count;
                    }
                }
            }
        }
    return number; 
    }

    public static void main(String[] args) {
            /*
    - leggyakoribb szám: 0-100 közötti véletlen számokkal töltsünk fel egy 300 elemű tömböt, majd keressük meg h melyik szám került bele a leggyakrabban{
      */
            randomArr(nums);
            System.out.println("The numbers: ");
            for (int i : nums) {
                System.out.print(i + " ");
        }
            System.out.printf("\nThe most common int is %d\n" , mostCom(nums) );
     }

}
