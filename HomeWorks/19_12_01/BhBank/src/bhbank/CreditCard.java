package bhbank;

public class CreditCard {
    
    private String cardNumber;
    private Loan creditCardLoan;
    
    public static final int CARD_REQUEST_FEE = 5_000;
    public static final double WITHDRAW_FEE = 0.1;
    public static final int MAX_WITHDRAW_FEE = 1000;
    

    public CreditCard(String cardNumber, Loan creditCardLoan) {
        this.cardNumber = cardNumber;
        this.creditCardLoan = creditCardLoan;
        
    }
    
    public void cardWithdraw(int amount ){
        int amountWithFees = Bank.calculateFee(amount, WITHDRAW_FEE);
        if (amountWithFees > MAX_WITHDRAW_FEE) {
            amountWithFees = MAX_WITHDRAW_FEE;
        }
        
        
    }

    public String getCardNumber() {
        return cardNumber;
    }

    public void setCardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
    }

    public Loan getCreditCardLoan() {
        return creditCardLoan;
    }

    public void setCreditCardLoan(Loan creditCardLoan) {
        this.creditCardLoan = creditCardLoan;
    }
    
    
    
    
    
   

}
