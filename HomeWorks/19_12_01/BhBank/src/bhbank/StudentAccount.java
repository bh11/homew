package bhbank;

/**
 *
 * @author momate
 */
public class StudentAccount extends BankAccount {

    public StudentAccount(String accountNumber) {
        super(accountNumber);
    }
    
    

    @Override
    public void transferMoney(int amount, BankAccount account) {
        if (getBalance() > amount) {
            account.setBalance(account.getBalance() + amount);
            setBalance(this.getBalance() - amount);
        }
    }

    @Override
    public void cashWithdrawal(int amount, CreditCard card) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String toString() {
        return "StudentAccount{"+ "Account Number: " + this.getAccountNumber() + ", Account Balance: " + this.getBalance() + '}';
    }


    
}
