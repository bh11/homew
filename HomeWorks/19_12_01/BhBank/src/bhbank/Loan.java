package bhbank;


public class Loan {

    private int loanAmount;

    public Loan( int loanAmount, BankAccount account) {
        this.loanAmount = loanAmount;
        addLoanAmountToBalance(account);
    }
    
    private void addLoanAmountToBalance(BankAccount account){
        account.setBalance(account.getBalance() + this.loanAmount);
    }

    public int getLoanAmount() {
        return loanAmount;
    }

    public void setLoanAmount(int loanAmount) {
        this.loanAmount = loanAmount;
    }
    
    
    
    
    
}
