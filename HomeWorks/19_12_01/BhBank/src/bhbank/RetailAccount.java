package bhbank;

public class RetailAccount extends BankAccount {

    private int accountManagmentFee;
    private final int discountCondition = 250_000;
    private final int basicFee = 1500;
    private final int discountedFee = 500;
    private final double transferFeeModifier = 0.05;

    public RetailAccount(String accountNumber) {
        super(accountNumber);

    }

    private int currentAccountManagmentFee() {
        return getBalance() > discountCondition ? discountedFee : basicFee;

    }

    @Override
    public void transferMoney(int amount, BankAccount account) {
        int amountWithFees = Bank.calculateFee(amount, transferFeeModifier);

        if (getBalance() > amountWithFees) {
            account.setBalance(account.getBalance() + amount);
            setBalance(this.getBalance() - amountWithFees);
        }

    }

    @Override
    public void cashWithdrawal(int amount, CreditCard card) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
       @Override
    public String toString() {
        return "RetailAccount{" + "Account Number: " + this.getAccountNumber() + ", Account Balance: " + this.getBalance() + '}';
    }

  
}
