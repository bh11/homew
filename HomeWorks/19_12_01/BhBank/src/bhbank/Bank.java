package bhbank;

import static bhbank.CreditCard.*;

public class Bank {

    public static final double VAT = 0.27;
    public static final int LEGAL_AGE = 18;
    
    
    public static void openNewAccount(User user){
        if (user.getAge() <= LEGAL_AGE) {
            user.setAccount(new StudentAccount(Integer.toString(Utiles.generateRandomNumber(9000, 10000))));
        }else{
        user.setAccount(new RetailAccount(Integer.toString(Utiles.generateRandomNumber(9000, 10000))));
        }
        
        user.getAccount().setBalance(- BankAccount.ACCOUNT_OPENING_FEE);
    
    }
    
    public static void makeNewCreditCard(User user){
        if (user.getAccount().getBalance() < (int)CARD_REQUEST_FEE * (1 + VAT)) {
            System.out.println("Not enough balance");
        }else{
        user.getAccount().setCreditCard( new CreditCard(Integer.toString(Utiles.generateRandomNumber(5000, 8000))));
        }
    }
    

    public static int calculateFee(int amount, double feeModifier) {
        return amount + (int)  (amount * feeModifier * (1 + VAT));
    }

}
