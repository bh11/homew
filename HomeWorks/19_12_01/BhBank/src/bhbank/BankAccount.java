package bhbank;

public abstract class BankAccount {

    private String accountNumber;
    public static final int ACCOUNT_OPENING_FEE = 500;
    private int balance;
    private CreditCard creditCard;

    
    public BankAccount(String accountNumber) {
        this.accountNumber = accountNumber;
        this.balance = 0;
    }
    
    public void moneyDeposit(int amount){
        setBalance(getBalance() + amount);
    
    }

    public abstract void transferMoney(int amount, BankAccount account);

    public abstract void cashWithdrawal(int amount, CreditCard card);
    

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public int getBalance() {
        return balance;
    }

    public void setBalance(int balance) {
        this.balance = balance;
    }

    public CreditCard getCreditCard() {
        return creditCard;
    }

    public void setCreditCard(CreditCard creditCard) {
        this.creditCard = creditCard;
        setBalance(getBalance() - CreditCard.CARD_REQUEST_FEE);
    }

}
