/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.employee.service;

import hu.braininghub.employee.repository.EmployeeDAO;
import hu.braininghub.employee.repository.dto.Employee;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import javax.ejb.Stateless;
import javax.inject.Inject;

/**
 *
 * @author momate
 */
@Stateless
public class EmployeeService {

    @Inject
    private EmployeeDAO dao;

    public List<String> getNames() {

        try {
            return dao.getEmployees().stream()
                    .map(Employee::getFirstName)
                    .collect(Collectors.toList());

        } catch (SQLException ex) {
            Logger.getLogger(EmployeeService.class.getName()).log(Level.SEVERE, null, ex);
        }

        return new ArrayList<>();
    }

    public List<Employee> searchNamesByInput(String str) {
        try {
            return dao.findByName(str);
        } catch (SQLException ex) {
            Logger.getLogger(EmployeeService.class.getName()).log(Level.SEVERE, null, ex);
        }

        return new ArrayList<>();
    }
}
