<%-- 
    Document   : filteredEmployees
    Created on : 2020.02.05., 19:22:58
    Author     : momate
--%>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>
<%@page import="hu.braininghub.employee.repository.dto.Employee"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Filtered Employees</title>
    </head>
    <body>
        <h2>Filtered Employees</h2>

        <table border = "1">

            <thead> 
                <tr>
                    <th>Fist Name</th>
                    <th>Last Name</th>
                    <th>Salary</th>
                </tr>

            </thead>
            <tbody>
            
                <c:forEach var="emp" items = "${employees}">
                    <c:choose>
                        <c:when test="${emp.salary < 10000}">
                            <tr>
                                <td><c:out value="${emp.firstName}"/></td>
                                <td><c:out value="${emp.lastName}"/></td>
                                <td><c:out value="${emp.salary} $"/></td>   
                            </tr>
                        </c:when>
                        <c:otherwise>
                            <td bgcolor="red"> <c:out value="${emp.firstName}"/></td>
                            <td bgcolor="red"> <c:out value="${emp.lastName}"/></td>
                            <td bgcolor="red"><c:out value="${emp.salary} $"/></td>
                            <td> <c:out value="WHAOOO"/></td>
                        </c:otherwise>
                    </c:choose>
                </c:forEach>

                </tbody>
            </table>
    </body>
</html>
