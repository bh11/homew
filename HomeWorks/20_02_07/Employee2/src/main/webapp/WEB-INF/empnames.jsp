<%-- 
    Document   : empnames
    Created on : 2020.01.31., 20:52:27
    Author     : momate
--%>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Employee List</title>
    </head>
    <body>
        First table:
        <table border = "1">
            <%
                List<String> names = (List<String>) request.getAttribute("names");
                for (String s : names) {
                    out.print("<tr><td>" + s + "</td></tr>");
                }


            %>

        </table>
            Second table:
            <table border = "1">
                <c:forEach var="name" items = "${names}">
                    <tr><td><c:out value="${name}"/></td></tr>
                </c:forEach>
                
            </table>
        <p>

        <form method="post" action="EmployeeServlet">
            <fieldset>
                <legend>Search employee </legend>
                <p>Employee Name: <input type="text" name="search" />  </p> 
                <input type="submit" value="Search employee"/>
            </fieldset>
        </form>
    </body>
</html>
