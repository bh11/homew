package animal_sound;

public class Animal {

    protected String name;
    protected int age;
    protected int countOfLegs;
    
    private Animal[] animals = new Animal[10];
    
    
    public Animal(Animal[] animals){
    this.animals = animals;
   }
    
    public Animal(String name,  int age, int countOfLegs){
    this.age = age;
    this.name = name;
    this.countOfLegs = countOfLegs;
    }
    
    public void say(){
        System.out.println("RRRRrrrrRRRrr");
    }
    
    public void printAllSound(){
        for (int i = 0; i < animals.length; i++) {
            animals[i].say();
        }
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public int getCountOfLegs() {
        return countOfLegs;
    }

    public void setCountOfLegs(int countOfLegs) {
        this.countOfLegs = countOfLegs;
    }
    
    
           
}
