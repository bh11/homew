package animal_sound;

public class App {

    public static void main(String[] args) {

        

        Dog d1 = new Dog("Buksi", 3, 4, "brown");
        Cat c1 = new Cat("Cirmi", 12, 3, true);
        Fox f1 = new Fox("Karak", 6, 4, true);
        Dog d2 = new Dog("Tapi", 6, 3, "black");
        Cat c2 = new Cat("Pompom", 1, 4, true);
        Fox f2 = new Fox("Sanyi", 2, 4, false);
        Dog d3 = new Dog("Szék", 1, 4, "transparent");
        Cat c3 = new Cat("Hari", 7, 4, false);
        
        Animal a = new Animal(new Animal[]{d1, c1, f1, d2, c2, f2, d3, c3 });
        
        a.printAllSound();
        

    }

}
