package animal_sound;

public class Cat extends Animal {
    
    private boolean domestic;

    public Cat(String name, int age, int countOfLegs, boolean domestic){
    super(name, age, countOfLegs);
    this.domestic = domestic;
    }
    
    @Override
    public void say(){
        System.out.println("Meow");
    }
    
            @Override
    public String toString() {
        return "This is a Cat who's has this attributes: " + "name=" + name + ", age=" + age  + ", countOfLegs=" + countOfLegs + ", domestic=" + domestic ;
    }

    public boolean isDomestic() {
        return domestic;
    }

    public void setDomestic(boolean domestic) {
        this.domestic = domestic;
    }
    
    
    
}
