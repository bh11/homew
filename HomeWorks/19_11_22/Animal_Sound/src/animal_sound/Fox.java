package animal_sound;

public class Fox extends Animal {
    
    boolean friendOfVuk;

    public Fox (String name, int age, int countOfLegs, boolean friendOfVuk ){
    super(name, age,countOfLegs);
    this.friendOfVuk = friendOfVuk;
    
    }
    
    @Override
    public void say(){
        System.out.println("Hatee-hatee-hatee-ho");
    }
    
            @Override
    public String toString() {
        return "This is a Fox who's has this attributes: " + "name=" + name + ", age=" + age  + ", countOfLegs=" + countOfLegs + ", friendOfVuk=" + friendOfVuk ;
    }
    
    public boolean getFriendOfVuk(){
    return friendOfVuk;
    }
    
    public void setFriendOfVuk(boolean friendOfVuk){
    this.friendOfVuk = friendOfVuk;
    }
}
