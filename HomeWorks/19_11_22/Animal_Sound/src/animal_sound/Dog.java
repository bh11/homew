package animal_sound;

public class Dog extends Animal{

    private String color;
    
    public Dog(String name, int age, int countOfLegs, String color) {
        super(name, age, countOfLegs);
        this.color = color;
        
    }
    
    @Override
    public void say(){
        System.out.println("Wauh");
    }
    
        @Override
    public String toString() {
        return "This is a Dog who's has this attributes: " + "name=" + name + ", age=" + age  + ", countOfLegs=" + countOfLegs + ", color=" + color ;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

}
