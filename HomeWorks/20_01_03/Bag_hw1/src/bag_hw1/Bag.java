package bag_hw1;

import java.util.AbstractList;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;

public class Bag<T>{

    private final List<T> t;

    public Bag() {
        t = new ArrayList<>();
    }
    
    public boolean add(T add){
        return t.add(add);
    }
    
    public boolean remove(T remove){
        return t.remove(remove);
    }

        
        
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        t.forEach(c -> sb.append(c.toString()).append(" \n"));
        return sb.toString();
    }

 
    
    
    
    
    
    
}
