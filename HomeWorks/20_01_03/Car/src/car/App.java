
package car;

public class App {

    public static void main(String[] args) {
        
        Car c1 = new Car.CarBuilder("Ferrari", "red").setHorsePower(1000).setIsASportCar(true).build();
        
        Car c2 = new Car.CarBuilder("Mustang", "black").setMaxSpeed(Integer.MAX_VALUE).build();
        
        System.out.println(c1.toString());
      
    }

}
