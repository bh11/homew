package car;

public class Car {

    private String name;
    private String color;

    private int maxSpeed;
    private int horsePower;
    private boolean isASportCar;

    public static class CarBuilder {

        private String name;
        private String color;

        private int maxSpeed;
        private int horsePower;
        private boolean isASportCar;

        public CarBuilder(String name, String color) {
            this.name = name;
            this.color = color;
        }

        public CarBuilder setName(String name) {
            this.name = name;
            return this;
        }

        public CarBuilder setColor(String color) {
            this.color = color;
            return this;
        }

        public CarBuilder setMaxSpeed(int maxSpeed) {
            this.maxSpeed = maxSpeed;
            return this;
        }

        public CarBuilder setHorsePower(int horsePower) {
            this.horsePower = horsePower;
            return this;
        }

        public CarBuilder setIsASportCar(boolean isASportCar) {
            this.isASportCar = isASportCar;
            return this;
        }
        
        public Car build(){
            return new Car(this);
        }
        
        
        

    }

    public Car(CarBuilder cb) {
        this.name = cb.name;
        this.color = cb.color;
        this.maxSpeed = cb.maxSpeed;
        this.horsePower = cb.horsePower;
        this.isASportCar = cb.isASportCar;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("Car - ");
        sb.append("Name: " + name);
        sb.append(" Color: " + color);
        if (this.maxSpeed != 0) {
            sb.append(" Max Speed: " + maxSpeed);
        }
        if (this.horsePower != 0) {
            sb.append("Horse Power: " + horsePower);
        }
        if (this.isASportCar == true) {
            sb.append(" This is a Sport Car");
        }
        
       return sb.toString();
    }
    
    

}
