package reader;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class ConsoleReader {

    private static final String STOP = "end";
    private static final String SAVE = "SAVE";
    private static final String CREATE = "CREATE";
    private boolean reading = true;
    private int counter = 0;

    LineParser lp = new LineParser();

    public void read() {

        try (BufferedReader br = new BufferedReader(new InputStreamReader(System.in))) {
            while (reading) {

                String operation = br.readLine();
                if (CREATE.equalsIgnoreCase(operation)) {
                    String creater = br.readLine();
                    lp.parse(creater);
                    counter++;
                }
                if (SAVE.equalsIgnoreCase(operation) || counter >100) {
                    reading = false;    

                }

            }
            lp.save();
            lp.report();

        } catch (IOException ex) {
            System.out.println("" + ex);
        }

    }

}
