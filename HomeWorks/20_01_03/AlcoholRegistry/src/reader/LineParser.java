package reader;

import drinks.AbstractDrink;
import drinks.DrinkFactory;
import java.util.ArrayList;
import java.util.List;
import report.Reporting;
import report.Saver;

public class LineParser {

    
    private static final String DELIMITER = ";";
    
    Reporting repo = new Reporting();
    Saver save = new Saver();
    
    private final List <AbstractDrink> drinkStore = new ArrayList<>();
    DrinkFactory df = new DrinkFactory();
    
    public void parse (String line){
        String[] parameteres = line.split(DELIMITER);
        AbstractDrink drink = df.createDrink(parameteres);
        drinkStore.add(drink);
    
    }
    
    public void report(){
        repo.report(drinkStore);
    }
    
    public void save(){
    save.serializeDrink(drinkStore);
    }

}
