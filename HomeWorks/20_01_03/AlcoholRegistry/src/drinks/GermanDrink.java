package drinks;

import utilies.labelGenerator;

public class GermanDrink extends AbstractDrink{
    
    private String labelColor;
    private String labelDate;
    
    labelGenerator generator = new labelGenerator();
    
    public GermanDrink(Type type, Producer producer, double alcoholPercentage, int price) {
        super(type, producer, alcoholPercentage, price);
        this.labelColor = generator.getLabelColor();
        this.labelDate = generator.getGeneratingDate().toString();
    }

    @Override
    public String toString() {
        return  "GermanDrink" + "{" + super.toString()  + "labelColor=" + labelColor + ", labelDate=" + labelDate + '}';
    }
    
    
    
    

}
