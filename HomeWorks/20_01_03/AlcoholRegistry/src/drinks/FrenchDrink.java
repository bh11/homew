package drinks;

import exceptions.FrozenException;

public class FrenchDrink extends AbstractDrink{
    
   private boolean isFrozen = false;
    
    public FrenchDrink(Type type, Producer producer, double alcoholPercentage, int price) {
        super(type, producer, alcoholPercentage, price);
    }
    
    public void transport() throws FrozenException{
        if (isFrozen) {
            throw new FrozenException();
        }else{
            System.out.println("Transporting in process");
        }
    }
    
    public void refrigeration(){
        this.isFrozen = true;
    }
    
    

}
