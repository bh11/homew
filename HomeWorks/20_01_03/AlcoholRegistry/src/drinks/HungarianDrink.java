package drinks;

public class HungarianDrink extends AbstractDrink{

    public HungarianDrink(Type type, Producer producer,  double alcoholPercentage, int price) {
        super(type, producer, alcoholPercentage, price);
    }
    
    public void drink(){
        System.out.println("Iszom, iszom, veled iszom");
    }
    
    public void makeFröccs(){
        setAlcoholPercentage(getAlcoholPercentage()/1.4);
    }

    @Override
    public String toString() {
        return "HungarianDrink " + super.toString();
    }
    
    

}
