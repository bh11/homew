package drinks;

public class DrinkFactory {
    
    public AbstractDrink createDrink(String[] parameters){
    
        String country = parameters[0];
        Producer producer = new Producer(country, parameters[2]);
        Type alcoholType = Type.valueOf(parameters[1].toUpperCase());
        double alcoholPercentage = Double.parseDouble(parameters[3]);
        int price = Integer.parseInt(parameters[4]);
        
        
        
        if ("german".equals(country.toLowerCase())) {
            return new GermanDrink(alcoholType, producer, alcoholPercentage, price);
        }else if("french".equalsIgnoreCase(country.toLowerCase())){
            return new FrenchDrink(alcoholType, producer, alcoholPercentage, price);
        }else{
        return new HungarianDrink(alcoholType, producer, alcoholPercentage, price);
        }
        
    }

}
