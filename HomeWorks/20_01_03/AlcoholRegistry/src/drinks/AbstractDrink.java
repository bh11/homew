package drinks;

import java.io.Serializable;
import utilies.idGenerator;

public class AbstractDrink implements Comparable<AbstractDrink> ,Serializable{

    private Type type;
    private Producer producer;
    private final int id;
    private double alcoholPercentage;
    private int price;
    
    idGenerator generator = new idGenerator();

    public AbstractDrink(Type type, Producer producer, double alcoholPercentage, int price) {
        this.type = type;
        this.producer = producer;
        this.id = generator.generateIdNumber();
        this.alcoholPercentage = alcoholPercentage;
        this.price = price;
    }

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }

    public Producer getProducer() {
        return producer;
    }

    public void setProducer(Producer producer) {
        this.producer = producer;
    }

    public double getAlcoholPercentage() {
        return alcoholPercentage;
    }

    public void setAlcoholPercentage(double alcoholPercentage) {
        this.alcoholPercentage = alcoholPercentage;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public int getId(){
        return id;
    }

    @Override
    public String toString() {
        return "AbstractDrink{" + "type=" + type + ", producer=" + producer + ", id=" + id + ", alcoholPercentage=" + alcoholPercentage + ", price=" + price + ", generator=" + generator + '}';
    }

    @Override
    public int compareTo(AbstractDrink id) {
       return this.id - id.getId();
    }


    
    
    
    

}
