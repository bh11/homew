package report;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.List;

public class Saver {
    
    private static final String FILE = "drink.ser";
    
    public void serializeDrink(List wine){
        try (ObjectOutputStream ou = new ObjectOutputStream(new FileOutputStream(FILE))){
             
             ou.writeObject(wine);
        } catch (FileNotFoundException ex) {
            System.out.println(ex);
        } catch (IOException ex) {
            System.out.println(ex);
        }
    }


}
