package report;

import com.sun.org.apache.bcel.internal.Repository;
import drinks.AbstractDrink;
import drinks.FrenchDrink;
import drinks.GermanDrink;
import drinks.HungarianDrink;
import drinks.Type;
import java.util.List;
import java.util.stream.Collectors;

public class Reporting {

//    Hány termék lett kimentve?
//Hány termék van speciális csoportonként?
//Hány palackozott bor van készleten?
//Kik a gyártók a készletet nézve (fel kell sorolni őket)?
    public void report(List<AbstractDrink> store) {

        System.out.println("Number of saved products is: " + numberOfSavedProduct(store));
        System.out.println("Number of Hungarian products: " + numberOfHungarianProducts(store));
        System.out.println("Number of German products: " + numberOfGermanProducts(store));
        System.out.println("Number of French products: " + numberOfFrenchProducts(store));
        System.out.println("Number of wine: " + numberOfWine(store));
        System.out.println("Producers of products: " + producerList(store));

    }

    private long numberOfSavedProduct(List<AbstractDrink> store) {

        return store.stream().count();
    }

    private long numberOfHungarianProducts(List<AbstractDrink> store) {
        return store.stream()
                .filter(f -> f instanceof HungarianDrink)
                .count();
    }

    private long numberOfGermanProducts(List<AbstractDrink> store) {
        return store.stream()
                .filter(f -> f instanceof GermanDrink)
                .count();

    }

    private long numberOfFrenchProducts(List<AbstractDrink> store) {
        return store.stream()
                .filter(f -> f instanceof FrenchDrink)
                .count();

    }

    private long numberOfWine(List<AbstractDrink> store) {
        return store.stream()
                    .filter(f -> Type.WINE.equals(f.getType()))
                    .count();
    }
    
    private List producerList(List<AbstractDrink> store){
          return store.stream()
                      .map(m -> m.getProducer().getName()).distinct().collect(Collectors.toList());
    }

}
