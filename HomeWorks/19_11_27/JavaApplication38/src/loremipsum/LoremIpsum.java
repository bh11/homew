package loremipsum;

public class LoremIpsum {

    public static int sentenceCounter(String str) {
        int count = 0;

        for (int i = 0; i < str.length() - 1; i++) {
            if (str.trim().charAt(i) == '.') {
                count++;
            }
        }
        return count;
    }

    public static int itCounter(String str) {
        int count = 0;
        for (int i = 0; i < str.length() - 1; i++) {
            if (str.substring(i, i + 2).equals("it")) {
                count++;
            }
        }

        return count;
    }

    public static String betweenIt(String str) {
        int first = str.indexOf("it");
        int last = 0;

        for (int i = 0; i < str.length() - 1; i++) {
            if (str.substring(i, i + 2).equals("it")) {
                last = i;

            }
        }

        return str.substring(first + 2, last);
    }

    public static String secondWord(String str) {
        String result = "";
        String words[] = str.split("\\s+");
        

        for (int i = 0; i < words.length; i = i + 2) {
            result+= (words[i] + " ");
        }
    

    return result ;
}

public static void main(String[] args) {

        String str = " Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.";
        StringBuilder sb = new StringBuilder(str);

        System.out.println("Number of sentences is: " + sentenceCounter(str));
        System.out.println("Number of \"it\" is: " + itCounter(str));
        System.out.println("The text between the first and last \"it\" is: " + betweenIt(str));        
        System.out.println("Every second word: " + secondWord(str));
        System.out.println(str.replace('a', 'A'));
        System.out.println("The string in reverse is : " + sb.reverse().toString());

    
    
    
    }
}
