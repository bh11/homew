package ability;

/**
 *
 * @author momate
 */
public interface Firing {
    
    void fire();
    
}
