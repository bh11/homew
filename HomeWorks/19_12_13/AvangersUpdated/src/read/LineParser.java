package read;

import exceptions.InvalidNameAvengersException;


import hero.AbstractHero;
import hero.HeroFactory;
import report.Reporting;
import report.Saver;
import store.Fleet;

public class LineParser {

    private static final String DELIMITER = ";";
    private static final int MIN_CHARACTERS_OF_NAME = 2;
    
    
    private final Fleet store = new Fleet();
    private final Reporting report = new Reporting(store);
    private final Saver save = new Saver();

    public void process(String line) throws InvalidNameAvengersException {
        String[] parameters = line.split(DELIMITER);
        checkNameRestriction(parameters[0]);
        AbstractHero hero = HeroFactory.create(parameters);
        store.add(hero);
    }

    public void print() {
        System.out.println(store.toString());
    }

    public void checkNameRestriction(String name) throws InvalidNameAvengersException {
        if (name.length() < MIN_CHARACTERS_OF_NAME) {
            throw new InvalidNameAvengersException("Invalid name: " + name);
        }
    }
    
    
    public void report(){
        report.numberOfBornOnEarthHeros(store);
        report.strongestHero(store);
        report.printIdCardNumbers(store);
    }
    
    


    
    public void save(){
        save.heroSaver;
    }

}
