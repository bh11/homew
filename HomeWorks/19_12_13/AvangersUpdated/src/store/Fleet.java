package store;

import com.sun.xml.internal.ws.policy.privateutil.PolicyUtils;
import hero.AbstractHero;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Fleet {

    private final List<Ship> ships = new ArrayList<>();

    public void add(AbstractHero hero) {
        findShip().add(hero);
    }

    @Override
    public String toString() {
        return "Fleet{" + "ships=" + ships + '}';
    }

    
    private Ship findLastShip() {

        if (ships.isEmpty()) {
            ships.add(new Ship());
        }
        return ships.get(ships.size() - 1);

    }

    private Ship findShip() {
        Ship ship = findLastShip();

        if (ship.isEmptySeat()) {
            return ship;
        } else {
            ships.add(new Ship());
            return ships.get(ships.size() - 1);
        }

    }

    public List<Ship> getShips() {
        return Collections.unmodifiableList(ships);
    }
    

}
