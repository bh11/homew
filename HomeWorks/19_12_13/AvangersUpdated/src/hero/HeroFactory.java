package hero;

import stone.StoneType;

public class HeroFactory {
    

    private static final String EARTH = "0";
    private static final String NOT_EARTH = "1";

    //név;erő;kő;földi-e
    //Captain America;3;SOUL;0
    public static AbstractHero create(String[] parameters) {
        String name = parameters[0];
        int power = Integer.parseInt(parameters[1]);
        StoneType stone = StoneType.valueOf(parameters[2]);

        if (EARTH.equals(parameters[3])) {
            return new BornOnEarth(name, power, stone, new IdentityCard());
        } else {
            return new NotBornOnEarth(name, power, stone);
        }
    }

}

