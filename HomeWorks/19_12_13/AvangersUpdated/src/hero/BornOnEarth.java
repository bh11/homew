package hero;

import ability.Flying;
import stone.StoneType;

public class BornOnEarth extends AbstractHero implements Flying {
    
    private final IdentityCard identCard;

    public BornOnEarth(final String name, int power, StoneType stone, IdentityCard identyCard) {
        super(name, power, stone);
        this.identCard = identyCard;
    }   

    @Override
    public void fly() {
        System.out.println("i can fly");
    }

    public IdentityCard getIdentCard() {
        return identCard;
    }

    @Override
    public String toString() {
        return super.toString() +  "BornOnErath{" + "identCard=" + identCard + '}';
    }
    
    


}
