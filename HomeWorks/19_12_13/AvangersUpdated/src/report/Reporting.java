package report;

import hero.AbstractHero;
import hero.BornOnEarth;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import stone.StoneType;
import store.Fleet;
import store.Ship;

public class Reporting {
    
    private Fleet fleet;

    public Reporting(Fleet fleet) {
        this.fleet = fleet;
    }
    
    private void printCountOfStonesByShip(){
        countOfStonesByShip().entrySet().stream()
                             .forEach(i -> {
                                 System.out.println("Ship: " + i);
                                 i.getValue().forEach((j,k) ->
                                 System.out.println("Stone: " + j + " Count: " + k));
                                         
                             }
                           );
    }
    
    
    private Map<StoneType,Integer> countOfStones(Ship ship){
       return ship.getHeroes().stream()
                  .map(AbstractHero::getStone)
                  .collect(Collectors.toMap(
                          k -> k, 
                          v -> 1, 
                          (v1,v2) -> v1 + v2, // ha ütközik a két kulcs hozzáadkuk az új értékét a régihez
                          HashMap::new
                  )
               );
    }
    
    private Map<Ship, Map<StoneType, Integer >> countOfStonesByShip(){
        return fleet.getShips().stream()
                .collect(Collectors.toMap(
                        s -> s, 
                        this::countOfStones, // v -> countOfStones(v)
                        (x,y) -> x           // ha ütközik két kulcs, akkor a régi értéket tartjuk meg
                )
             );
    }

    public void numberOfBornOnEarthHeros(Fleet fi) {
        fi.getShips().stream().map(Ship::getHeroes).filter(h -> h instanceof BornOnEarth).count();
    }

    public void strongestHero(Fleet fi) {
        fi.getShips().stream()
                .flatMap(s -> s.getHeroes().stream())
                .max((h1, h2) -> h1.getPower() - h2.getPower())
                .get()
                .getName();

    }

    public void printIdCardNumbers(Fleet fi) {
        fi.getShips().stream()
                .map(c -> c.getHeroes())
                .filter(h -> h instanceof BornOnEarth)
                .forEach(m -> System.out.println(((BornOnEarth)m).getIdentCard().getNumber()));
        
    }

    private boolean containsOnlyBornOnErath(Ship ship){
        return ship.getHeroes().stream().allMatch( p -> p instanceof BornOnEarth);
    }
    
    private Map<Boolean , List<Ship>> partitonInByCotainsOnlyBornOnEarth(){
        return fleet.getShips().stream()
                .collect(Collectors.partitioningBy(this::containsOnlyBornOnErath) //partitioningBy -> két mapra bontja
             );
    }
    
    private long numberOfShipsContainingOnlyBornOnEarth(){
        return partitonInByCotainsOnlyBornOnEarth().get(Boolean.TRUE).size();
    }
}
        