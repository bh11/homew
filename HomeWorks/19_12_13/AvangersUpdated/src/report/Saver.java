package report;
import java.io.FileWriter
        ;
import java.io.IOException;
import java.util.List;
import hero.AbstractHero;
import java.io.FileOutputStream;
import java.io.ObjectOutputStream;

public class Saver {
    
    public static void heroSaver(List<AbstractHero> heroes, String fileName){
        try (FileOutputStream fs = new FileOutputStream(fileName)){
            ObjectOutputStream ou = new ObjectOutputStream(fs);
            
             for (AbstractHero hero : heroes) {
                ou.writeObject(hero);
             }
            
            
            
        }catch (IOException e){
            System.out.println(e);
        }
    }

}

