
package commandline;

import reader.ConsoleReader;

public class CommandLine {

    public static void main(String[] args) {
      
        ConsoleReader reader = new ConsoleReader();
        reader.read();
    }

}
