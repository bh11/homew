package parser;

public class Commands {
    
    public static final String CHANGE_DIRECTORY = "cd";
    public static final String LIST = "ls";
    public static final String PARENT_DIRECTORY = "..";
    public static final String RENAME = "mv";
    public static final String PWD = "pwd";
  
    
    private Commands(){
    }
    
}