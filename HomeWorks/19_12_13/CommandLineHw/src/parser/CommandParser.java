package parser;

import directory.MyDirectory;
import static parser.Commands.*; // statikusan importálni metódust vagy változót

public class CommandParser {
    
    private static final String DELIMETER = " ";
    private MyDirectory directory = new MyDirectory();
    
    public void parse(String line){
        String[] commands = line.split(DELIMETER);
        
        if (PWD.equals(commands[0])) {
            directory.pwd();
        }else if(commands.length == 2 && CHANGE_DIRECTORY.equals(commands[0])){
            directory.cd(commands[1]);
        }else if (LIST.equals(commands[0])){
            directory.ls();
        }else if(commands.length == 3 && RENAME.equals(commands[0])){
            directory.mv(commands[1], commands[2]);
        }
    }

}
