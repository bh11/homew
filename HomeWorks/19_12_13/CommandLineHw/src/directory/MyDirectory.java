package directory;

import java.io.File;
import java.io.IOException;

import static parser.Commands.*;

public class MyDirectory {
    
    
    
    private File file = new File(System.getProperty("user.dir"));
    
    public void pwd(){
        try {  
            System.out.println(file.getCanonicalPath());
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }
    
    public void ls(){
       File[] files = file.listFiles();
        for (File f : files) {
            System.out.print(f.getName());
            System.out.println(f.isFile() ? " F " + f.length() : " D");
            
        }
    }
    
    public void cd(String command){
        if (PARENT_DIRECTORY.endsWith(command)) {
            cdParent();
        }else{
            cdDirectory(command);
        }
    }
    
    public void mv(String from, String to){
     File[] files = file.listFiles();
        for (File fi : files) {
            if (fi.getName().equals(from)) {
                File newFile = new File(to);
                fi.renameTo(newFile);

            }
        }
     
    }
    
    private void cdParent(){
        
        file = file.getParentFile();
    }
    
    private void cdDirectory(String directory){
        File to = new File(file, directory);
        
        if (to.exists() && to.isDirectory()) {
            file = to;
        }
    }

}
