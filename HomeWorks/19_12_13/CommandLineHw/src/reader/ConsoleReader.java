package reader;

import java.util.Scanner;
import parser.CommandParser;

public class ConsoleReader {
    
    private static final String STOP = "exit";
    
    public void read(){
        CommandParser parser = new CommandParser();
        try (Scanner sc = new Scanner(System.in)){
            String line;
            do {
                line = sc.nextLine();
                if (line != null) {
                    parser.parse(line);
                }
            } while (!STOP.equals(line));
            
        }
    }

}
