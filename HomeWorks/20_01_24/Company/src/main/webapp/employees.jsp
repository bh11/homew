<%-- 
    Document   : employees
    Created on : 2020.01.22., 17:24:50
    Author     : momate
--%>

<%@page import="com.mycompany.Employee"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <h2>Employee List</h2>
    <body>
       
        <%List<Employee> employees = (List<Employee>) request.getAttribute("employees"); %>
        
        <table>
            <thead> 
                <tr>
                    <th>Name</th>
                    <th>Address</th>
                    <th>Salary</th>
                    <th>Department Name</th>
                </tr>
                    
            </thead>
            <tbody>
                <% for (int i = 0; i < employees.size(); i++) {%>
                <tr>
                    <td> <%= employees.get(i).getName()%></td>
                    <td> <%= employees.get(i).getAddress()%></td>
                    <td> <%= employees.get(i).getSalary()%></td>
                    <td> <%= employees.get(i).getDepratmentName()%> </td>
                </tr>
                <% }%>
            </tbody>
             
        </table>
    </body>
    <h2>Search List</h2>
    
    <h2>Add new employee </h2>
    <form method="post" action="AddEmployeeServlet">
        <fieldset>
            <legend>Add employee </legend>
            <p>Name: <input type="text" name="name" />  </p> 
            <p>Address: <input type="text" name="address" /> </p> 
            <p>Salary: <input type="number" name="salary" />  </p> 
            <p>Department Name: <input type="text" name="departmentName" /> </p>
            <input type="submit" value="Add employee"/>
        </fieldset>
    </form>
    <h2>Delete employee </h2>
    <form method="post" action="DeleteEmployeeServlet">
        <fieldset>
            <legend>Add employee </legend>
            <p>Name: <input type="text" name="name" />  </p> 
            <input type="submit" value="Delete employee"/>
        </fieldset>
    </form>
        <h2>Search employee by Department Name</h2>
    <form method="post" action="SearchEmployeeServlet">
        <fieldset>
            <legend>Search employee </legend>
            <p>Department Name: <input type="text" name="departmentName" />  </p> 
            <input type="submit" value="Search employee"/>
        </fieldset>
    </form>
    </body>
</html>
