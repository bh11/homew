<%-- 
    Document   : departments
    Created on : 2020.01.22., 20:51:12
    Author     : momate
--%>

<%@page import="com.mycompany.Department"%>
<%@page import="java.util.List"%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <h2>Department List</h2>
    <body>
        <%List<Department> departments = (List<Department>) request.getAttribute("departments");%>
        <table>
            <thead>
                <tr>
                    <th>Departmen Name</th>
                    <th>Address</th>
                    <th>Maximum capacity</th>
                </tr>
            </thead>
            <tbody>
                <% for (int i = 0; i < departments.size(); i++) {%>
                <tr>
                    <td> <%= departments.get(i).getName()%></td>
                    <td> <%= departments.get(i).getAddress()%></td>
                    <td> <%= departments.get(i).getMaxEmployeeNumber()%></td>
                </tr>
                <%}%>
            </tbody>
        </table>
    </body>

    <h2>Add new Department </h2>
    <form method="post" action="addDepartmentServlet" >
        <fieldset>
            <legend>Add Department </legend>
            <p>Name: <input type="text" name="name" />  </p> 
            <p>Address: <input type="text" name="address" /> </p> 
            <p>Maximum capacity <input type="number" name="maxEmployeeNumber" />  </p> 
            <input type="submit" value="Add Department"/>
        </fieldset>
    </form>
</html>
