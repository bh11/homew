/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author momate
 */
public class SearchEmployeeServlet extends HttpServlet {
    
    EmployeeService employeeService = new EmployeeService();
            


    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        this.employeeService.searchEmployeeByDepartmentId(request.getParameter("departmentName"));
        
        request.setAttribute("employees", this.employeeService.getSearchedEmployees());
        request.getRequestDispatcher("employees.jsp").forward(request, response);
       
    }

}
