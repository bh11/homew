package com.mycompany;

import java.util.ArrayList;
import java.util.List;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author momate
 */
public class EmployeeService {

    private static List<Employee> searchedEmployees = new ArrayList<>();
    private static List<Employee> employees = new ArrayList<>();

    public static void addEmployee(Employee employee) {
        employees.add(employee);
    }

    public static void deleteEmployeeByName(String name) {
        int index = getIndexByName(name);
        if (index != -1) {
            employees.remove(index);
        }
    }

    public static void searchEmployeeByDepartmentId(String departmentName) {
        searchedEmployees.clear();
        for (int i = 0; i < employees.size(); i++) {
            if (departmentName.equals(employees.get(i).getDepratmentName())) {
                searchedEmployees.add(employees.get(i));
            }
        }
    }

    public static List<Employee> getEmployees() {
        return employees;

    }

    public static List<Employee> getSearchedEmployees() {
        return searchedEmployees;
    }

    private static int getIndexByName(String name) {
        for (int i = 0; i < employees.size(); i++) {
            if (name.equals(employees.get(i).getName())) {
                return i;
            }

        }
        return -1;
    }

}
