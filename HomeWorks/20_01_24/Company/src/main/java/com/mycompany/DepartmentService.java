/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author momate
 */
public class DepartmentService {
    
        private static List<Department> departments = new ArrayList<>();

    public static void addDepartment(Department department) {
        departments.add(department);
    }
    

    public static List<Department> getDepartments() {
        return departments;

    }
    
    private static int getIndexByName(String name){
        for (int i = 0; i < departments.size(); i++) {
            if (name.equals(departments.get(i).getName())) {
                return i;
            }
            
        }
        return -1;
    }

    
}
