/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany;

/**
 *
 * @author momate
 */
public class Employee {
    
    private String name;
    private String address;
    private int salary;
    private String depratmentName;

    public Employee(String name, String address, int salary, String depratmentName) {
        this.name = name;
        this.address = address;
        this.salary = salary;
        this.depratmentName = depratmentName;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public int getSalary() {
        return salary;
    }

    public void setSalary(int salary) {
        this.salary = salary;
    }

    public String getDepratmentName() {
        return depratmentName;
    }

    public void setDepratmentName(String depratmentName) {
        this.depratmentName = depratmentName;
    }

    
    
}
