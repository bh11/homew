/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany;

/**
 *
 * @author momate
 */
public class Department {
    
    private String name;
    private String address;
    private int maxEmployeeNumber;

    public Department(String name, String address, int maxEmployeeNumber) {
        this.name = name;
        this.address = address;
        this.maxEmployeeNumber = maxEmployeeNumber;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public int getMaxEmployeeNumber() {
        return maxEmployeeNumber;
    }

    public void setMaxEmployeeNumber(int maxEmployeeNumber) {
        this.maxEmployeeNumber = maxEmployeeNumber;
    }
    
    
    
    
}
