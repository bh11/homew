package enumandsort.project2;

import java.util.Arrays;


public class SortedCollection {

    public void sortStringsByLength(String[] arr) {
        Arrays.sort(arr,
                (String a, String b) -> a.length() - b.length());
        print("Shortest to longest: ", arr);
    }

    public void sortStringsByLengthReverse(String[] arr) {
        Arrays.sort(arr,
                (String a, String b) -> b.length() - a.length());
        print("Longest to shortest: ", arr);

    }

    public void sortStringAlphabetically(String[] arr) {
        Arrays.sort(arr,
                (String a, String b) -> a.charAt(0) - b.charAt(0));
        print("Sort by aplhabetically: ", arr);
    }

    public void cointainsESort(String[] arr) {
        Arrays.sort(arr,
                (String a, String b) -> (a.contains("e") ? 0 : 1) - (b.contains("e") ? 0 : 1));
        print("Sort by string has e: ", arr);
    }

    private void print(String str, String[] arr) {
        System.out.println("\n"+ str);
        for (String s : arr) {
            System.out.println(s);
        }
    }

}
