/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package repository;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import lombok.Data;

/**
 *
 * @author momate
 */
@Data
@Entity
@Table(name = "patient")
public class Patient implements Serializable{

    public static final int SerialVersionUID = 42;
    @Id
    @Column(name = "taj_id")
    private int tajId;
    @Column(name = "name")
    private String name;
    @Column(name = "address")
    private String address;
    @Column(name = "year_of_birth")
    private String yearOfBirth;
    @OneToMany
    private List<HealthCare> patientCares;
    
   
    
    
    
}
