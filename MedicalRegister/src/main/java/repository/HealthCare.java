/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package repository;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import lombok.Data;

/**
 *
 * @author momate
 */
@Data
@Entity
@Table(name = "health_care")
public class HealthCare implements Serializable {

    public static final int SerialVersionUID = 42;
    @Id
    @Column(name = "provider_name")
    private String providerName;
    @Column(name = "provider_address")
    private String providerAddress;
    @Column(name = "care_date")
    private String date;
    @Column(name = "care_name")
    private String careName;
    @Column(name = "aid")
    private int aid;
    @Column(name = "doctor_name")
    private String doctorName;
     @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "taj_id", nullable = false, insertable = true, updatable = true)
    private Patient tajId;

}
