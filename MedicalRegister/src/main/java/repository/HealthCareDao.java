/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package repository;

import java.util.List;
import javax.ejb.Singleton;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author momate
 */
@Singleton
public class HealthCareDao {

    @PersistenceContext
    private EntityManager em;

    public List<HealthCare> getPatientHelathCaresByTajId(int id) {
        return em.createQuery("SELECT e FROM HealthCare e WHERE e.tajId := id", HealthCare.class)
                .setParameter("id", id)
                .getResultList();

    }

}
