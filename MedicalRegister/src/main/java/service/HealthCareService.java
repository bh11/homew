/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service;

import dto.HealthCareDto;
import exception.NoHealthCareFoundException;
import java.lang.reflect.InvocationTargetException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.Singleton;
import javax.inject.Inject;
import org.apache.commons.beanutils.BeanUtils;
import repository.HealthCare;
import repository.HealthCareDao;

/**
 *
 * @author momate
 */
@Singleton
public class HealthCareService {

    @Inject
    private HealthCareDao dao;

    public List<HealthCareDto> getPatientHelathCaresByTajId(int id) throws NoHealthCareFoundException {

        List<HealthCare> temp = dao.getPatientHelathCaresByTajId(id);
        List<HealthCareDto> result = null;

        if (temp.isEmpty()) {
            throw new NoHealthCareFoundException();
        } else {
            for (HealthCare healthCare : temp) {
                HealthCareDto dto = new HealthCareDto();
                try {
                    BeanUtils.copyProperties(healthCare, dto);
                    result.add(dto);
                } catch (IllegalAccessException | InvocationTargetException ex) {
                    Logger.getLogger(HealthCareService.class.getName()).log(Level.SEVERE, null, ex);
                }

            }
            return result;

        }
    }

}
