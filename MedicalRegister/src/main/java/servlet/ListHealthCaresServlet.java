/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlet;

import dto.HealthCareDto;
import exception.NoHealthCareFoundException;
import java.io.IOException;
import java.util.List;
import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import service.HealthCareService;

/**
 *
 * @author momate
 */
@WebServlet(name = "ListHealthCaresServlet", urlPatterns = "/ListHealthCaresServlet")
public class ListHealthCaresServlet extends HttpServlet {
    
    @Inject
    private HealthCareService service;
    
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) 
            throws ServletException, IOException{
        
    }
    

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        int id = Integer.parseInt(request.getParameter("tajId"));
        try {
            List<HealthCareDto> result = service.getPatientHelathCaresByTajId(id);
           request.setAttribute("healtCareList", result); 
        } catch (NoHealthCareFoundException ex) {
           request.setAttribute("errMsg", ex.getMessage());
        }
        
        request.getRequestDispatcher("healthCares.jsp").forward(request, response);
        
    }

}
