<%-- 
    Document   : index
    Created on : 2020.04.27., 18:18:39
    Author     : momate
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
        <title>Adatok</title>
    </head>
    <body>
        <div class="container">
            <div id="q0">
                
                    <div style="display:list-item">     <!-- *** FLUID: begin -->

                        <center>
                            <h2>Kérem, adja meg a lekérdezni kívánt időszak kezdő és záró dátumát!</h2>

                            <p class="font-weight-normal">A dátumoknál az éééé.hh.nn. formátumot használja. Pl. a 2016 március 2-i dátum esetén a beírandó szöveg: 2016.03.02.</p>

                        </center>
                        <div class="my-4">
                            <form name="data" action="fnrun.jsp" method="POST">

                                <div class="form-group row">
                                    <label for="datum1" class="col-6 col-form-label text-right">Időszak kezdete:</label>
                                    <div class="col-4">
                                        <input class="form-control datum" id="datum1" type="text" name="tol" size="11" maxlength="11" placeholder="éééé.hh.nn."> 
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="datum2um1" class="col-6 col-form-label text-right">Időszak vége:</label>
                                    <div class="col-4">
                                        <input class="form-control datum" id="datum2" type="text" name="ig" size="11" maxlength="11" placeholder="éééé.hh.nn.">
                                    </div>
                                </div>

                                <input type="hidden" name="fname" value="273406">

                                <div class="form-group row">
                                    <div class="col-6"></div>
                                    <div class="col-4">
                                        <button class="btn btn-primary mb-2 mr-3 text-right" type="submit" onclick="return checkDate();">Lekérdezés</button>
                                    </div>
                                </div>

                            </form>
                        </div>

                        <p class="my-5 font-weight-normal">
                            Tisztelt Felhasználó!
                            <br><br>
                            Felhívjuk szíves figyelmét, hogy a Nemzeti Egészségbiztosítási Alapkezelő adatbázisában nem szerepelnek egészségügyi dokumentációk (pl. leletek, zárójelentések, beteglapok) amelyek bár az Ön ellátáshoz kapcsolódhatnak, de a társadalombiztosítás finanszírozásához nem köthetők. Ezek a dokumentumok továbbra is csak az Ön által igénybevett ellátás helyén, illetleg a házi- vagy kezelőorvosi betegdokumentációban lelhetők fel.  
                        </p>






                    </div>
                    <button id="next" >next</button>
               

            </div>

            <div id="q1" style="display:none" >
                
                    <h3>2. Paper comes from... </h3>
                    <input type="radio" name="question1" value="A"/>Water<br/>
                    <input type="radio" name="question1" value="B"/>Cement<br/>
                    <input type="radio" name="question1" value="C"/>Trees<br/>
                    <input type="radio" name="question1" value="D"/>The Sky<br/>
                    <button id="next" >neeext</button>
                

            </div>
        </div>



        <script>

            document.getElementById('next').addEventListener("click", function () {

                var listContainer = document.getElementById("container");
                var listItem = listContainer.getElementsByTagName("div");

                for (var i = 0; i < listItem.length - 1; i++)
                {
                    if (listItem[i].style.display == "list-item")
                    {
                        listItem[i].style.display = "none";
                        listItem[i + 1].style.display = "list-item";
                        if (i == listItem.length - 2)
                            this.disabled = "true";
                        break;
                    }
                }

            });
        </script>

    </body>

</html>
