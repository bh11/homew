package hu.braininghub.exception;


import org.springframework.web.bind.annotation.ResponseStatus;

import static org.springframework.http.HttpStatus.*;

@ResponseStatus(code = BAD_REQUEST)
public class CantDivideByZeroException extends RuntimeException{

    public CantDivideByZeroException(String msg){
        super(msg);
    }


}
