package hu.braininghub.exception;

import org.springframework.web.bind.annotation.ResponseStatus;

import static org.springframework.http.HttpStatus.BAD_REQUEST;

@ResponseStatus(code = BAD_REQUEST)
public class UnsupportedOperationException extends RuntimeException{

    public UnsupportedOperationException(String msg){
        super(msg);
    }
}
