package hu.braininghub.controller;


import hu.braininghub.exception.CantDivideByZeroException;
import hu.braininghub.exception.UnsupportedOperationException;
import hu.braininghub.requestmodel.CalculatorModel;
import hu.braininghub.service.CalculatorService;
import org.slf4j.LoggerFactory;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


@RestController
public class CalculatorController {

    Logger logger = LoggerFactory.getLogger(CalculatorController.class);

    @Autowired
    CalculatorService service;

    @RequestMapping(value = "/calculate",
            method = RequestMethod.POST,
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity calculate(@RequestBody CalculatorModel model) {

        Integer fst = Integer.parseInt(model.getFirst());
        Integer sec = Integer.parseInt(model.getSecond());
        String op = model.getOperator();

        logger.error(fst+ " " + op + " " + sec);
        Integer result;
        try {
            result = service.calculate(fst, sec, op);
        }catch (CantDivideByZeroException | UnsupportedOperationException ex){
            return ResponseEntity.ok(ex.getMessage());
        }



        return ResponseEntity.ok(result);
    }
}
