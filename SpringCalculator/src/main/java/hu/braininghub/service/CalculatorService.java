package hu.braininghub.service;

import hu.braininghub.exception.CantDivideByZeroException;
import hu.braininghub.exception.UnsupportedOperationException;
import org.springframework.stereotype.Service;

@Service
public class CalculatorService implements Calc {

    @Override
    public Integer calculate(Integer firstNumber, Integer secondNumber, String operator)
            throws CantDivideByZeroException, UnsupportedOperationException {


        switch (operator) {

            case "+":
                return add(firstNumber, secondNumber);
            case "-":
                return subtract(firstNumber, secondNumber);
            case "*":
                return multiply(firstNumber, secondNumber);
            case "/":
                if (firstNumber == 0) {
                    return 0;
                } else if (secondNumber == 0) {
                    throw new CantDivideByZeroException("Can't divide by zero");

                } else {
                    return divide(firstNumber, secondNumber);
                }
            default:

                throw new UnsupportedOperationException("Unsupported operation is given for calculation");
        }

    }


    private int add(Integer first, Integer second) {
        return first + second;
    }

    private int subtract(Integer first, Integer second) {
        return first - second;
    }

    private int multiply(Integer first, Integer second) {
        return first * second;
    }

    private int divide(Integer first, Integer second) {

        return first / second;
    }
}
