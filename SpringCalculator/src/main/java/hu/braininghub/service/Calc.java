package hu.braininghub.service;

public interface Calc {

    Integer calculate(Integer firstNumber, Integer secondNumber, String operator);
}
