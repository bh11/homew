package hu.braininghub.requestmodel;

import java.io.Serializable;

public class CalculatorModel implements Serializable {

    private String first;
    private String second;
    private String operator;

    public String getFirst() throws NullPointerException {
        return first;
    }

    public void setFirst(String first) {
        this.first = first;
    }

    public String getSecond() {
        return second;
    }

    public void setSecond(String second) {
        this.second = second;
    }

    public String getOperator() {
        return operator;
    }

    public void setOperator(String operator) {
        this.operator = operator;
    }
}
