package bh11_191020_2;

import java.util.Scanner;


public class Bh11_191020_2 {


    public static void main(String[] args) {

        //switch
        
        Scanner sc = new Scanner (System.in);
        System.out.println("Add meg az osztályzatod: ");
        int jegy = sc.nextInt();
        if (sc.hasNextInt()){    // érdemes megnézni hogy egyáltalán tartalmazza -e a megfeleló értéket
        switch(jegy){
            case 1: System.out.println("Elégtelen");
            break;
            case 2: System.out.println("Elégséges");
            break;
            case 3 : System.out.println("Közepes");
            break;
            case 4 : System.out.println("Jó");
            break;
            case 5: System.out.println("Jeles");
                                   
        }
        
        if (jegy == 1){
            System.out.println("Elégtelen");
        }else if(jegy == 2){
            System.out.println("Elégséges");
        }else if (jegy == 3){
            System.out.println("Közepes");
        }else if (jegy == 4){
            System.out.println("Jó");
        }else{
            System.out.println("Jeles");
        }

        }
    }

}
