package bh11_191020_5;

import java.util.Scanner;


public class Bh11_191020_5 {


    public static void main(String[] args) {
        
        Scanner sc = new Scanner (System.in);
        
        System.out.println("Add meg a magasságot: ");
        int high= sc.nextInt();
        if (high < 0){
            System.out.println("Túl kevés");
        }else if (high<=200){
            System.out.println("Alföld");
        }else if (high <= 500){
            System.out.println("Dombság");
        }else if (high <= 1500){
            System.out.println("Középhegység");
        }else{
            System.out.println("Hegység");
        }

    }

}
