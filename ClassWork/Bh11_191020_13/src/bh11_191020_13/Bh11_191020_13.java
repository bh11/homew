package bh11_191020_13;

import java.util.Scanner;


public class Bh11_191020_13 {


    public static void main(String[] args) {
        
        // bekérni 2 zámot és a kisebbtől a nagyobb felé a 7-re végződőket kiírni
        Scanner sc = new Scanner (System.in);
        boolean isValidInput = false;
        int number1 = -1;
        
        // egyik szám beolvasása
    do {
    System.out.println("Adj meg egy egész számot: ");
    if(sc.hasNextInt()){
    number1 = sc.nextInt();
    isValidInput = true;
    }else{
    sc.next();
    }
    }while(!isValidInput);
    
    // másik szám bekérése
    
     
    int number2 = -1;
       do {
    System.out.println("Adj meg egy egész számot: ");
    if(sc.hasNextInt()){
    number2 = sc.nextInt();
    
    }else{
    sc.next();
    }
    }while(!isValidInput);
       
       // mi - max
       
       int from = number1 > number2 ? number2 : number1;
       int to = number1 > number2 ? number1 : number2;
       
       // 7-re végződő kiíratása
       
       int i = from;
       while (i <=to){
       if (i%10 == 7){
           System.out.println(i);
       }
       i++;
       }
    
    }

}
