package bh11_191020_8;

import java.util.Scanner;


public class Bh11_191020_8 {


    public static void main(String[] args) {
        Scanner sc = new Scanner (System.in);
        // do while ciklus
        
        do{
            System.out.println("Ez mindeképpen lefut");
        }while(false);
        
        // Addig kérünk be adatokat amíg 0-t nem kapunk / if-el megakadájozzuk hogy a 0-t is kiírja
        
        int stop_number = 0;
        int number;
        
        do{
        number=sc.nextInt();
        if (number != stop_number){
            System.out.println(number);
        }
        }while ( number != stop_number);
    }

}
