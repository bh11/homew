package linkedlistm;

public class LinkedList<E> {

    Link head;

    transient int size = 0;

    public boolean insert(E e) {
        Link link = new Link();
        link.data = e;
        link.next = null;

        if (head == null) {

            head = link;
        } else {
            Link l = head;
            while (l.next != null) {
                l = l.next;

            }
            l.next = link;
        }

        size++;
        return true;

    }

    public int size() {
        return size;
    }
    
    public void show(){
        System.out.println(display());
    }

    private String display() {
        Link tmp = head;
        String result = "[";
        while (tmp != null) {
            result += " - " + tmp.data.toString();
            tmp = tmp.next;

        }
        result += "]";
        return result;
    }

}
