package linkedlistm;

public class Link<E> {
    
    public E data;
    
    public Link next;

    public Link() {
    }
    
    

    public Link(E data, Link next) {
        this.data = data;
        this.next = next;
    }  
     
}
