import { ImagesService } from './../images.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'image-fetch',
  templateUrl: './image-fetch.component.html',
  styleUrls: ['./image-fetch.component.css']
})
export class ImageFetchComponent implements OnInit {

  searchTerm: string;
  loading = false;
  images :any[];

  constructor(private imagesService: ImagesService) { }

  ngOnInit(): void {
  }


  onSubmit(){
    this.loading = true;
    this.images = [];

    this.imagesService.search(this.searchTerm)
    .subscribe((result : any) => {
      this.images = result['results'];
      this.loading = false;
    });
    
  }

}
